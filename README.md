# git-eca-rest-api

---

**NOTE**

This project was migrated to [Eclipse Gitlab](https://gitlab.eclipse.org/eclipsefdn/it/api/git-eca-rest-api) on October 14, 2021.

---

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

<!-- TOC -->
- [git-eca-rest-api](#git-eca-rest-api)
  - [About validation](#about-validation)
    - [What is a valid commit?](#what-is-a-valid-commit)
    - [Submitting applications for existing bots](#submitting-applications-for-existing-bots)
    - [Enabling commit hook in GitLab](#enabling-commit-hook-in-gitlab)
    - [Configure environment](#configure-environment)
    - [Update your Host file](#update-your-host-file)
  - [Application setup and operation](#application-setup-and-operation)
    - [Setting up the application for local use](#setting-up-the-application-for-local-use)
    - [Running the application in dev mode](#running-the-application-in-dev-mode)
    - [Packaging and running the application](#packaging-and-running-the-application)
  - [Private Project Reports](#private-project-reports)
    - [Usage](#usage)
      - [Usage Params](#usage-params)
      - [Report Schema](#report-schema)
<!-- /TOC -->

## About validation

### What is a valid commit?

To be considered a valid commit, the following set of rules are checked against all commits that are submitted to this service. If any fail and the commit is related to a project, the service returns a message indicating the commit is not suitable for submission along with messages as hints.

1. All users that commit or author changes within a project within the Eclipse space must have a signed [ECA](https://accounts.eclipse.org/user/eca), and therefore, Eclipse accounts.
    - The one exception to this rule is registered bot users, as they cannot sign an [ECA](https://accounts.eclipse.org/user/eca).
    - Users covered by an MCCA are also covered, as it is considered an equivalent document in the eyes of contribution access.
2. Requests made to non-project repositories by default will be allowed to pass with warnings to enable [ECA](https://accounts.eclipse.org/user/eca) validation to be run across a system with projects or repositories not directly managed by the Eclipse Foundation, such as forks or supporting projects ([#26](https://github.com/EclipseFdn/git-eca-rest-api/issues/26)).
    - Within the API, a request can be made under 'strict mode' to enforce contribution guidelines within repositories not covered by an active project ([#43](https://github.com/EclipseFdn/git-eca-rest-api/pull/43)).

While these rules apply to all project repositories, any non-project repositories will also be checked. The messages indicate the failures as warnings to the consuming service (like Gitlab or Gerrit) unless 'strict mode' is enabled for the service. Whether or not a repository is tracked (or if it is a project repository) is determined by its presence as a repository directly linked to an active project within the [PMI](https://projects.eclipse.org/), as reported by the [Projects API](https://api.eclipse.org/#tag/Projects).

### Submitting applications for existing bots

To submit requests for existing bots to be registered within our API (such as dependabot), please see the [Project Bots API repository](https://github.com/EclipseFdn/projects-bots-api) and create an issue.

### Enabling commit hook in GitLab

To enable the Git hook that makes use of this service, a running GitLab instance will be needed with shell access. This instruction set assumes that the running GitLab instance runs using the Omnibus set up rather than the source. For the differences in process, please see the [GitLab custom hook administration instructions](https://docs.gitlab.com/ee/administration/custom_hooks.html). Once obtained, the following steps can be used to start or update the hook.

1. Access the GitLab server shell, and create a folder at `/opt/gitlab/embedded/service/gitlab-shell/hooks/pre-receive.d/` if it doesn't already exist. This folder will contain all of the servers global Git hooks for pre-receive events. These hooks trigger when a user attempts to push information to the server.
1. In the host machine, copy the ECA script to the newly created folder. If using a docker container, this can be done with a call similar to the following:
`docker cp src/main/rb/eca.rb gitlab.eca_web_1:/opt/gitlab/embedded/service/gitlab-shell/hooks/pre-receive.d/`

1. In the GitLab shell once again, ensure that the newly copied script matches the folders ownership, and that the file permissions are `755`. This allows the server to properly run the hook when needed.

### Configure environment

Export the following environment variables. Note that you might need to alter the values based on your environment.

```bash
# Update settings based on your enviroment.
export MARIADB_PASSWORD=""
export MARIADB_USERNAME=""
export MARIADB_HOST=mariadb
export MARIADB_PORT=3306
```

### Update your Host file

Different operating system, different file paths!

Windows: C:\Windows\System32\drivers\etc\hosts
Linux / MacOS: /etc/hosts

```bash
127.0.0.1 api.eclipse.dev.docker
127.0.0.1 gitlab.eclipse.dev.docker
```

## Application setup and operation

### Setting up the application for local use

Pre-requisites:

- Make
- Maven (apt install maven)
- Java 11 > (apt install openjdk-11-jdk)
- Eclipse Account API credentials
- Running MariaDB instance
- create config/application/secret.properties by copying config/application/secret.properties.sample. You must information edit the file before starting the server.

To build and start the server:

```bash
make compile-start
```

To build and start with a running Gitlab instance:

```bash
make compile-start-full
```

### Running the application in dev mode

You can run your application in dev mode that enables live coding using:

```bash
make dev-start
```

### Packaging and running the application

The application is packageable using `./mvnw package`.
It produces the executable `git-eca-rest-api-0.0.1-runner.jar` file in `/target` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/lib` directory.

The application is now runnable using `java -jar target/git-eca-rest-api-0.0.1-runner.jar`.

## Private Project Reports

### Usage

The private project reports are accessed through `/git/reports/gitlab/private-projects` and can be filtered using various parameters.

#### Usage Params

| Parameter name | Required | Accepts | Default | Description |
|----------------|:--------:|---------|---------|-------------|
| key | ✓ | string | N/A | The private access token required to use this endpoint  |
| status | x | string | N/A | The project status. Either 'active' or 'deleted' |
| since | x | datestamp (ISO 8601), ex: 2022-10-11 | N/A | Starting date stamp for getting a slice of tracked private projects |
| until | x | datestamp (ISO 8601), ex: 2022-10-11 | N/A | Ending date stamp for getting a slice of tracked private projects |

#### Report Schema

The reports endpoint will return a list of objects with relevant info on tracked private projects.
The `Volatile` column denotes whether the property is subject to change once initially tracked.

| Property name | Type | Volatile | Description |
|---------------|------|:--------:|-------------|
| user_id | integer | x | The project creator's Gitlab userid. |
| project_id | integer | x | The project's Gitlab id. |
| project_path | string | ✓ | The project's path with namespace. Subject to change. |
| ef_username | string | ✓ | The project creator's EF username. Subject to change. |
| parent_project | integer/null | ✓ | Denotes whether the project has a parent, which indicates a fork. However, the user can remove the relation to the parent project. |
| creation_date | string | x | The project's creation date. |
| deletion_date | string/null | ✓ | The project's deletion date. Subject to change. |
