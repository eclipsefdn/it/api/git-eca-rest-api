/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Tests for Qute template extensions
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class EclipseQuteTemplateExtensionsTest {

    /**
     * Success cases where emails can be obfuscated and their expected output
     * 
     * 
     * <ul>
     * <li>Case 1: Standard address, no edge cases
     * <li>Case 2: Address with email alias (+123)
     * <li>Case 3: Multiple top-level domain parts
     * </ul>
     * 
     * @param input the value to be obfuscated
     * @param expected the value that is expected out
     */
    @ParameterizedTest
    @CsvSource(value = { "sample@co.co,sample@c* DOT co", "sample.address+123@eclipse.org,sample.address+123@ecl*pse DOT org",
            "sample@somecorp.co.co, sample@some*orp DOT co DOT co" })
    void obfuscateEmail_success(String input, String expected) {
        Assertions.assertEquals(expected, EclipseQuteTemplateExtensions.obfuscateEmail(input));
    }

    /**
     * Cases where a value can't be obfuscated and an empty string is returned.
     * 
     * <ul>
     * <li>Case 1: No @ symbol
     * <li>Case 2: Null value
     * <li>Case 3: Empty string
     * <li>Case 4: No qualified domain (something.com)
     * </ul>
     * 
     * @param input the value to be obfuscated
     * @param expected the value that is expected out
     */
    @ParameterizedTest
    @CsvSource(value = { "no-at-symbol.com,''", ",''", "'',''", "no-qualified-domain@wowee,''" })
    void obfuscateEmail_failure_invalidAddress(String input, String expected) {
        Assertions.assertEquals(expected, EclipseQuteTemplateExtensions.obfuscateEmail(input));
    }
}
