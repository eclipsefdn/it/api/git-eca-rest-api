/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*	      Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.resource;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.GitUser;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.namespace.APIStatusCode;
import org.eclipsefoundation.git.eca.namespace.ProviderType;
import org.eclipsefoundation.git.eca.test.namespaces.SchemaNamespaceHelper;
import org.eclipsefoundation.http.exception.ApplicationException;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.OidcSecurity;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response.Status;

/**
 * Tests for verifying end to end validation via the endpoint. Uses restassured to create pseudo requests, and Mock API endpoints to ensure
 * that all data is kept internal for test checks.
 *
 * @author Martin Lowe
 * @author Zachary Sabourin
 */
@QuarkusTest
class ValidationResourceTest {
    public static final String ECA_BASE_URL = "/eca";
    public static final String LOOKUP_URL = ECA_BASE_URL + "/lookup?q={param}";
    public static final String LOOKUP_LEGACY_URL = ECA_BASE_URL + "/lookup?email={param}";
    public static final String STATUS_URL = ECA_BASE_URL + "/status/{fingerprint}";
    public static final String STATUS_UI_URL = STATUS_URL + "/ui";

    /*
     * USERS
     */
    public static final GitUser USER_WIZARD = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();
    public static final GitUser USER_GRUNTS = GitUser.builder().setName("Grunts McGee").setMail("grunt@important.co").build();
    public static final GitUser USER_BARSHALL = GitUser
            .builder()
            .setName("Barshall Blathers")
            .setMail("slom@eclipse-foundation.org")
            .build();
    public static final GitUser USER_RANDO = GitUser.builder().setName("Rando Calressian").setMail("rando@nowhere.co").build();
    public static final GitUser USER_NEWBIE = GitUser.builder().setName("Newbie Anon").setMail("newbie@important.co").build();

    /*
     * BOTS
     */
    public static final GitUser BOT_PROJBOT = GitUser.builder().setName("projbot").setMail("1.bot@eclipse.org").build();
    public static final GitUser BOT_PROJ_GH = GitUser.builder().setName("protobot-gh").setMail("2.bot-github@eclipse.org").build();
    public static final GitUser BOT_PROTOBOT = GitUser.builder().setName("protobot").setMail("2.bot@eclipse.org").build();
    public static final GitUser BOT_PROTO_GH = GitUser.builder().setName("protobot-gh").setMail("2.bot-github@eclipse.org").build();
    public static final GitUser BOT_SPECBOT = GitUser.builder().setName("specbot").setMail("3.bot@eclipse.org").build();

    /*
     * BODY PARAMS
     */
    private static final Map<String, Object> SUCCESS_BODY_PARAMS = Map.of("passed", true, "errorCount", 0);
    private static final Map<String, Object> FAIL_SINGLE_ERR_BODY_PARAMS = Map.of("passed", false, "errorCount", 1);
    private static final Map<String, Object> FAIL_DOUBLE_ERR_BODY_PARAMS = Map.of("passed", false, "errorCount", 2);

    // Often used commit
    public static final Commit SUCCESS_COMMIT_WIZARD = createStandardUsercommit(USER_WIZARD, USER_WIZARD);

    // Best case request body
    public static final ValidationRequest VALIDATE_SUCCESS_BODY = createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample",
            Arrays.asList(SUCCESS_COMMIT_WIZARD));

    /*
     * BASIC VALIDATE CASES
     */
    public static final EndpointTestCase VALIDATE_SUCCESS_CASE = TestCaseHelper
            .prepareTestCase(ECA_BASE_URL, new String[] {}, SchemaNamespaceHelper.VALIDATION_RESPONSE_SCHEMA_PATH)
            .setBodyValidationParams(SUCCESS_BODY_PARAMS)
            .build();

    public static final EndpointTestCase VALIDATE_FORBIDDEN_CASE = TestCaseHelper
            .prepareTestCase(ECA_BASE_URL, new String[] {}, SchemaNamespaceHelper.VALIDATION_RESPONSE_SCHEMA_PATH)
            .setStatusCode(403)
            .setBodyValidationParams(FAIL_SINGLE_ERR_BODY_PARAMS)
            .build();

    /*
     * LOOKUP CASES
     */
    // by email
    public static final EndpointTestCase LOOKUP_SUCCESS_CASE = TestCaseHelper
            .buildSuccessCase(LOOKUP_URL, new String[] { "slom@eclipse-foundation.org" }, "");
    public static final EndpointTestCase LOOKUP_ANONYMOUS_CASE = TestCaseHelper
            .prepareTestCase(LOOKUP_URL, new String[] { "slom@eclipse-foundation.org" }, "")
            .setStatusCode(Status.BAD_REQUEST.getStatusCode())
            .build();
    public static final EndpointTestCase LOOKUP_FORBIDDEN_CASE = TestCaseHelper
            .buildForbiddenCase(LOOKUP_URL, new String[] { "newbie@important.co" }, "");
    public static final EndpointTestCase LOOKUP_NOT_FOUND_CASE = TestCaseHelper
            .buildNotFoundCase(LOOKUP_URL, new String[] { "dummy@fake.co" }, "");
    // by username
    public static final EndpointTestCase LOOKUP_USERNAME_SUCCESS_CASE = TestCaseHelper
            .buildSuccessCase(LOOKUP_URL, new String[] { "barshall_blathers" }, "");
    public static final EndpointTestCase LOOKUP_USERNAME_FORBIDDEN_CASE = TestCaseHelper
            .buildForbiddenCase(LOOKUP_URL, new String[] { "newbieAnon" }, "");
    public static final EndpointTestCase LOOKUP_USERNAME_NOT_FOUND_CASE = TestCaseHelper
            .buildNotFoundCase(LOOKUP_URL, new String[] { "dummy11" }, "");

    // lookup cases with email param
    public static final EndpointTestCase LOOKUP_SUCCESS_EMAIL_PARAM_CASE = TestCaseHelper
            .buildSuccessCase(LOOKUP_LEGACY_URL, new String[] { "slom@eclipse-foundation.org" }, "");
    public static final EndpointTestCase LOOKUP_ANONYMOUS_EMAIL_PARAM_CASE = TestCaseHelper
            .prepareTestCase(LOOKUP_LEGACY_URL, new String[] { "slom@eclipse-foundation.org" }, "")
            .setStatusCode(Status.UNAUTHORIZED.getStatusCode())
            .build();
    public static final EndpointTestCase LOOKUP_FORBIDDEN_EMAIL_PARAM_CASE = TestCaseHelper
            .buildForbiddenCase(LOOKUP_LEGACY_URL, new String[] { "newbie@important.co" }, "");
    public static final EndpointTestCase LOOKUP_NOT_FOUND_EMAIL_PARAM_CASE = TestCaseHelper
            .buildNotFoundCase(LOOKUP_LEGACY_URL, new String[] { "dummy@fake.co" }, "");

    @Inject
    CachingService cs;
    @Inject
    ObjectMapper json;

    @BeforeEach
    void cacheClear() {
        // if dev servers are run on the same machine, some values may live in the cache
        cs.removeAll();
    }

    @Test
    void validate() {
        EndpointTestBuilder.from(VALIDATE_SUCCESS_CASE).doPost(VALIDATE_SUCCESS_BODY).run();
    }

    @Test
    void validate_success_responseFormat() {
        EndpointTestBuilder.from(VALIDATE_SUCCESS_CASE).doPost(VALIDATE_SUCCESS_BODY).andCheckFormat().run();
    }

    @Test
    void validate_success_validResponseSchema() {
        EndpointTestBuilder.from(VALIDATE_SUCCESS_CASE).doPost(VALIDATE_SUCCESS_BODY).andCheckSchema().run();
    }

    @Test
    void validate_success_validRequestFormat() {
        String in;

        try {
            in = json.writeValueAsString(VALIDATE_SUCCESS_BODY);
        } catch (JsonProcessingException e) {
            throw new ApplicationException("Error converting body to JSON format", e);
        }

        Assertions.assertTrue(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.VALIDATION_REQUEST_SCHEMA_PATH).matches(in));
    }

    @Test
    void validateMultipleCommits() {
        List<Commit> commits = new ArrayList<>();

        commits.add(SUCCESS_COMMIT_WIZARD);

        Commit c2 = Commit
                .builder()
                .setAuthor(USER_GRUNTS)
                .setCommitter(USER_GRUNTS)
                .setBody("Signed-off-by: Grunts McGee<grunt@important.co>")
                .setHash("c044dca1847c94e709601651339f88a5c82e3cc7")
                .setSubject("Add in feature")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c2);

        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample", commits))
                .run();
    }

    @Test
    void validateMergeCommit() {
        Commit c1 = Commit
                .builder()
                .setAuthor(USER_RANDO)
                .setCommitter(USER_RANDO)
                .setBody(String.format("Signed-off-by: %s <%s>", USER_RANDO.getName(), USER_RANDO.getMail()))
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10", "46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c11"))
                .build();

        // No errors expected, should pass as only commit is a valid merge commit
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateCommitNoSignOffCommitter() {
        Commit c1 = Commit
                .builder()
                .setAuthor(USER_GRUNTS)
                .setCommitter(USER_GRUNTS)
                .setBody("")
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things")
                .setParents(Collections.emptyList())
                .build();

        // Should be valid as Grunt is a committer on the prototype project
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/prototype", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateCommitNoSignOffNonCommitter() {
        Commit c1 = Commit
                .builder()
                .setAuthor(USER_WIZARD)
                .setCommitter(USER_WIZARD)
                .setBody("")
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things")
                .setParents(Collections.emptyList())
                .build();

        // Should be valid as wizard has signed ECA
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/prototype.git", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateCommitInvalidSignOff() {
        Commit c1 = Commit
                .builder()
                .setAuthor(USER_BARSHALL)
                .setCommitter(USER_BARSHALL)
                .setBody(String.format("Signed-off-by: %s <%s>", USER_BARSHALL.getName(), "barshallb@personal.co"))
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things")
                .setParents(Collections.emptyList())
                .build();

        // Should be valid as signed off by footer is no longer checked
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/prototype.git", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateCommitSignOffMultipleFooterLines_Last() {
        Commit c1 = Commit
                .builder()
                .setAuthor(USER_BARSHALL)
                .setCommitter(USER_BARSHALL)
                .setBody(String
                        .format("Change-Id: 0000000000000001\nSigned-off-by: %s <%s>", USER_BARSHALL.getName(), USER_BARSHALL.getMail()))
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things")
                .setParents(Collections.emptyList())
                .build();

        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/prototype", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateCommitSignOffMultipleFooterLines_First() {
        Commit c1 = Commit
                .builder()
                .setAuthor(USER_BARSHALL)
                .setCommitter(USER_BARSHALL)
                .setBody(String
                        .format("Signed-off-by: %s <%s>\nChange-Id: 0000000000000001", USER_BARSHALL.getName(), USER_BARSHALL.getMail()))
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things")
                .setParents(Collections.emptyList())
                .build();

        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/prototype", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateCommitSignOffMultipleFooterLines_Multiple() {
        Commit c1 = Commit
                .builder()
                .setAuthor(USER_BARSHALL)
                .setCommitter(USER_BARSHALL)
                .setBody(String
                        .format("Change-Id: 0000000000000001\\nSigned-off-by: %s <%s>\nSigned-off-by: %s <%s>", USER_BARSHALL.getName(),
                                USER_BARSHALL.getMail(), USER_BARSHALL.getName(), "barshallb@personal.co"))
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things")
                .setParents(Collections.emptyList())
                .build();

        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/prototype", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateWorkingGroupSpecAccess() {
        // CASE 1: WG Spec project write access valid
        ValidationRequest vr = createGitHubRequest(false, "http://www.github.com/eclipsefdn-tck/tck-proto",
                Arrays.asList(SUCCESS_COMMIT_WIZARD));

        // Should be valid as Wizard has spec project write access + is committer
        EndpointTestBuilder.from(VALIDATE_SUCCESS_CASE).doPost(vr).run();

        // CASE 2: No WG Spec proj write access
        Commit c1 = createStandardUsercommit(USER_GRUNTS, USER_GRUNTS);

        vr = createGitHubRequest(false, "http://www.github.com/eclipsefdn-tck/tck-proto", Arrays.asList(c1));

        // Should be invalid as Grunt does not have spec project write access
        // Should have 2 errors, as both users get validated
        Map<String, Object> bodyParams = new HashMap<>(FAIL_DOUBLE_ERR_BODY_PARAMS);
        bodyParams.put("commits." + c1.getHash() + ".errors[0].code", APIStatusCode.ERROR_SPEC_PROJECT.getValue());

        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ECA_BASE_URL, new String[] {}, "")
                        .setStatusCode(403)
                        .setBodyValidationParams(bodyParams)
                        .build())
                .doPost(vr)
                .run();
    }

    /*
     * ECA SIGNATURE STATE TESTS
     */

    @Test
    void validateNoECA_author() {
        Commit c1 = createStandardUsercommit(USER_NEWBIE, USER_WIZARD);

        // Error should be singular + that there's no ECA on file
        EndpointTestBuilder
                .from(VALIDATE_FORBIDDEN_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateNoECA_committer() {
        Commit c1 = createStandardUsercommit(USER_WIZARD, USER_NEWBIE);

        // Error count should be 1 for just the committer access
        EndpointTestBuilder
                .from(VALIDATE_FORBIDDEN_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateNoECA_both() {
        Commit c1 = createStandardUsercommit(USER_NEWBIE, USER_NEWBIE);

        ValidationRequest vr = createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample", Arrays.asList(c1));

        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ECA_BASE_URL, new String[] {}, "")
                        .setStatusCode(403)
                        .setBodyValidationParams(FAIL_DOUBLE_ERR_BODY_PARAMS)
                        .build())
                .doPost(vr)
                .run();
    }

    @Test
    void validateAuthorNoEclipseAccount() {
        Commit c1 = createStandardUsercommit(USER_RANDO, USER_GRUNTS);

        // Error should be singular + that there's no Eclipse Account on file for author
        // Status 403 (forbidden) is the standard return for invalid requests
        EndpointTestBuilder
                .from(VALIDATE_FORBIDDEN_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateCommitterNoEclipseAccount() {
        Commit c1 = createStandardUsercommit(USER_GRUNTS, USER_RANDO);

        // Error should be singular + that there's no Eclipse Account on file for committer
        // Status 403 (forbidden) is the standard return for invalid requests
        EndpointTestBuilder
                .from(VALIDATE_FORBIDDEN_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateProxyCommitUntrackedProject() {
        Commit c1 = createStandardUsercommit(USER_GRUNTS, USER_RANDO);

        // Should be valid as project is not tracked
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample-not-tracked", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validate_ignoredRepo_success() {
        // rando is not on the TCK project, so this would fail for a tracked repo, but not an ignored repo
        Commit c1 = createStandardUsercommit(USER_RANDO, USER_RANDO);

        // Should be valid as repo is ignored
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn-tck/tck-ignored", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validate_ignoredRepo_failure_strictMode() {
        // rando is not on the TCK project, so this would fail in strict mode
        Commit c1 = createStandardUsercommit(USER_RANDO, USER_RANDO);

        // Should be valid as repo is ignored
        EndpointTestBuilder
                .from(VALIDATE_FORBIDDEN_CASE)
                .doPost(createGitHubRequest(true, "http://www.github.com/eclipsefdn-tck/tck-ignored", Arrays.asList(c1)))
                .run();
    }

    @Test
    void testValidate_success_noCommits() {

        // We do not block contributions to non-project repos
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(true, "http://www.github.com/eclipsefdn/prototype.git", Collections.emptyList()))
                .run();

        // Strictmode shouldn't affect the result
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/prototype.git", Collections.emptyList()))
                .run();
    }

    @Test
    void testValidate_failure_noRepoUrl() {
        Commit c1 = createStandardUsercommit(USER_WIZARD, USER_NEWBIE);

        // We do not block contributions to non-project repos
        EndpointTestBuilder.from(VALIDATE_SUCCESS_CASE).doPost(createGitHubRequest(true, "", Arrays.asList(c1))).run();

        // Strictmode shouldn't affect the result
        EndpointTestBuilder.from(VALIDATE_SUCCESS_CASE).doPost(createGitHubRequest(false, "", Arrays.asList(c1))).run();
    }

    @Test
    void testValidate_failure_noProvider() {
        Commit c1 = createStandardUsercommit(USER_WIZARD, USER_NEWBIE);

        // Request with no provider set
        ValidationRequest req = ValidationRequest
                .builder()
                .setStrictMode(true)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/prototype.git"))
                .setCommits(Arrays.asList(c1))
                .build();

        // We do not block contributions to non-project repos
        EndpointTestBuilder.from(VALIDATE_SUCCESS_CASE).doPost(req).run();

        req = req.toBuilder().setStrictMode(false).build();

        // Strictmode shouldn't affect the result
        EndpointTestBuilder.from(VALIDATE_SUCCESS_CASE).doPost(req).run();
    }

    /*
     * BOT ACCESS TESTS
     */

    @Test
    void validateBotCommiterAccessGithub() {
        Commit c1 = createNoBodyCommit(BOT_PROJBOT, BOT_PROJBOT);

        // Should be valid as bots should only commit on their own projects (including aliases)
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateBotCommiterAccessGithub_untracked() {
        Commit c1 = createNoBodyCommit(BOT_PROJBOT, BOT_PROJBOT);

        // Should be valid as bots can commit on any untracked project (legacy support)
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample-untracked", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateBotCommiterAccessGithub_invalidBot() {
        Commit c1 = createNoBodyCommit(BOT_PROJ_GH, BOT_PROJ_GH);

        // Should be invalid as bots should only commit on their own projects (including aliases)
        EndpointTestBuilder
                .from(VALIDATE_FORBIDDEN_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateBotCommiterAccessGithub_wrongEmail() {
        Commit c1 = createNoBodyCommit(BOT_PROTOBOT, BOT_PROTOBOT);

        // Should be invalid as wrong email was used for bot (uses Gerrit bot email)
        EndpointTestBuilder
                .from(VALIDATE_FORBIDDEN_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateBotCommiterAccessGitlab() {
        Commit c1 = createNoBodyCommit(BOT_PROTO_GH, BOT_PROTO_GH);

        // Should be valid as bots should only commit on their own projects (including aliases)
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitLabRequest(false, "https://gitlab.eclipse.org/eclipse/dash-second/dash.handbook.test", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateBotCommiterAccessGitlab_ignored() {
        Commit c1 = createNoBodyCommit(BOT_PROTO_GH, BOT_PROTO_GH);

        // Should be valid as bots can commit on any untracked project (legacy support)
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitLabRequest(false, "https://gitlab.eclipse.org/eclipse/dash/mirror/dash.handbook.untracked",
                        Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateBotCommiterAccessGitlab_invalidBot() {
        Commit c1 = createNoBodyCommit(BOT_SPECBOT, BOT_SPECBOT);

        // Should be invalid as bots should only commit on their own projects
        EndpointTestBuilder
                .from(VALIDATE_FORBIDDEN_CASE)
                .doPost(createGitLabRequest(false, "https://gitlab.eclipse.org/eclipse/dash-second/dash.handbook.test", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateBotCommiterAccessGitlab_wrongEmail() {
        Commit c1 = createNoBodyCommit(BOT_SPECBOT, BOT_SPECBOT);

        // Should be valid as wrong email was used, but is still bot email alias (uses Gerrit bot email)
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitLabRequest(false, "https://gitlab.eclipse.org/eclipse/dash/dash.git", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateBotCommiterAccessGerrit() {
        Commit c1 = createNoBodyCommit(BOT_PROTOBOT, BOT_PROTOBOT);

        // Should be valid as bots should only commit on their own projects (including aliases)
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGerritRequest(true, "/gitroot/sample/gerrit.other-project", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateBotCommiterAccessGerrit_untracked() {
        Commit c1 = createNoBodyCommit(BOT_PROTOBOT, BOT_PROTOBOT);

        // Should be valid as bots can commit on any untracked project (legacy support)
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGerritRequest(true, "/gitroot/sample/untracked.project", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateBotCommiterAccessGerrit_invalidBot() {
        Commit c1 = createNoBodyCommit(BOT_SPECBOT, BOT_SPECBOT);

        // Should be invalid as bots should only commit on their own projects (wrong project)
        EndpointTestBuilder
                .from(VALIDATE_FORBIDDEN_CASE)
                .doPost(createGerritRequest(true, "/gitroot/sample/gerrit.other-project", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateBotCommiterAccessGerrit_aliasEmail() {
        Commit c1 = createNoBodyCommit(BOT_PROTO_GH, BOT_PROTO_GH);

        // Should be valid as wrong email was used, but is still bot email alias
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGerritRequest(true, "/gitroot/sample/gerrit.other-project", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateNullEmailCheck() {
        // set up test users - uses GH (instead of expected Gerrit/LDAP email)
        Commit c1 = createNoBodyCommit(BOT_PROTO_GH, GitUser.builder().setName("protobot-gh").build());

        // Should be invalid as there is no email (refuse commit, not server error)
        EndpointTestBuilder
                .from(VALIDATE_FORBIDDEN_CASE)
                .doPost(createGerritRequest(true, "/gitroot/sample/gerrit.other-project", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validate_githubExternalId_success() {
        GitUser g1 = GitUser.builder().setName("grunter").setMail("other-grunt@test.co").setExternalId("grunter2").build();
        Commit c1 = createNoBodyCommit(g1, g1);

        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/prototype", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validate_githubExternalId_success_fallback() {
        GitUser g1 = GitUser.builder().setName("grunter").setMail("grunt@important.co").setExternalId("grunter").build();
        Commit c1 = createNoBodyCommit(g1, g1);

        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGitHubRequest(false, "http://www.github.com/eclipsefdn/prototype", Arrays.asList(c1)))
                .run();
    }

    /*
     * NO REPLY TESTS
     */

    @Test
    void validateGithubNoReply_legacy() {
        GitUser g1 = GitUser.builder().setName("grunter").setMail("grunter2@users.noreply.github.com").build();

        Commit c1 = createNoBodyCommit(g1, g1);

        // Should be valid as grunter used a no-reply Github account and has a matching GH handle
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGerritRequest(true, "/gitroot/sample/gerrit.other-project", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateGithubNoReply_success() {
        // sometimes the user ID and user name are reversed
        GitUser g1 = GitUser.builder().setName("grunter").setMail("123456789+grunter2@users.noreply.github.com").build();

        Commit c1 = createNoBodyCommit(g1, g1);

        // Should be valid as grunter used a no-reply Github account and has a matching GH handle
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGerritRequest(true, "/gitroot/sample/gerrit.other-project", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateGithubNoReply_nomatch() {
        GitUser g1 = GitUser.builder().setName("some_guy").setMail("123456789+some_guy@users.noreply.github.com").build();

        Commit c1 = createNoBodyCommit(g1, g1);

        // Should be invalid as no user exists with "Github" handle that matches some_guy
        EndpointTestBuilder
                .from(VALIDATE_FORBIDDEN_CASE)
                .doPost(createGerritRequest(true, "/gitroot/sample/gerrit.other-project", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateGithubNoReply_nomatch_legacy() {
        GitUser g1 = GitUser.builder().setName("some_guy").setMail("some_guy@users.noreply.github.com").build();

        Commit c1 = createNoBodyCommit(g1, g1);

        // Should be invalid as no user exists with "Github" handle that matches some_guy
        EndpointTestBuilder
                .from(VALIDATE_FORBIDDEN_CASE)
                .doPost(createGerritRequest(true, "/gitroot/sample/gerrit.other-project", Arrays.asList(c1)))
                .run();
    }

    /*
     * ALLOW LIST TESTS
     */

    @Test
    void validateAllowListAuthor_success() {
        GitUser g1 = GitUser.builder().setName("grunter").setMail("grunter2@users.noreply.github.com").build();
        GitUser g2 = GitUser.builder().setName("grunter").setMail("noreply@github.com").build();

        Commit c1 = createNoBodyCommit(g2, g1);

        // Should be valid as grunter used a no-reply Github account and has a matching GH handle
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGerritRequest(true, "/gitroot/sample/gerrit.other-project", Arrays.asList(c1)))
                .run();
    }

    @Test
    void validateAllowListCommitter_success() {
        GitUser g1 = GitUser.builder().setName("grunter").setMail("grunter2@users.noreply.github.com").build();
        GitUser g2 = GitUser.builder().setName("grunter").setMail("noreply@github.com").build();

        Commit c1 = createNoBodyCommit(g1, g2);

        // Should be valid as grunter used a no-reply Github account and has a matching GH handle
        EndpointTestBuilder
                .from(VALIDATE_SUCCESS_CASE)
                .doPost(createGerritRequest(true, "/gitroot/sample/gerrit.other-project", Arrays.asList(c1)))
                .run();
    }

    /*
     * DB PERSISTENCE TESTS
     */
    @Test
    void validateRevalidation_success() {
        ValidationRequest vr = createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample", Arrays.asList(SUCCESS_COMMIT_WIZARD));

        EndpointTestBuilder.from(VALIDATE_SUCCESS_CASE).doPost(vr).run();

        Map<String, Object> bodyParams = new HashMap<>(SUCCESS_BODY_PARAMS);
        bodyParams.put("commits." + SUCCESS_COMMIT_WIZARD.getHash() + ".messages[0].code", APIStatusCode.SUCCESS_SKIPPED.getValue());

        // repeat call to test that skipped status is passed
        EndpointTestBuilder
                .from(TestCaseHelper.prepareTestCase(ECA_BASE_URL, new String[] {}, "").setBodyValidationParams(bodyParams).build())
                .doPost(vr)
                .run();
    }

    @Test
    void validateRevalidation_errors() {
        Commit c1 = createStandardUsercommit(USER_NEWBIE, USER_WIZARD);

        ValidationRequest vr = createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample", Arrays.asList(c1));

        // should fail with 1 error
        EndpointTestBuilder.from(VALIDATE_FORBIDDEN_CASE).doPost(vr).run();

        Map<String, Object> bodyParams = new HashMap<>(FAIL_SINGLE_ERR_BODY_PARAMS);
        bodyParams.put("commits." + c1.getHash() + ".errors[0].code", APIStatusCode.ERROR_AUTHOR.getValue());

        // repeat call to test that previously run check still fails
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ECA_BASE_URL, new String[] {}, "")
                        .setStatusCode(403)
                        .setBodyValidationParams(bodyParams)
                        .build())
                .doPost(vr)
                .run();
    }

    @Test
    void validateRevalidation_partialSuccess() {
        List<Commit> commits = new ArrayList<>();

        // successful commit
        commits.add(SUCCESS_COMMIT_WIZARD);

        // error commit
        Commit c2 = createStandardUsercommit(USER_NEWBIE, USER_WIZARD);
        commits.add(c2);

        ValidationRequest vr = createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample", commits);

        // should fail with 1 error
        EndpointTestBuilder.from(VALIDATE_FORBIDDEN_CASE).doPost(vr).run();

        Map<String, Object> bodyParams = new HashMap<>(FAIL_SINGLE_ERR_BODY_PARAMS);
        bodyParams.put("commits." + c2.getHash() + ".errors[0].code", APIStatusCode.ERROR_AUTHOR.getValue());
        bodyParams.put("commits." + SUCCESS_COMMIT_WIZARD.getHash() + ".messages[0].code", APIStatusCode.SUCCESS_SKIPPED.getValue());

        // repeat call to test that previously run check still fails
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ECA_BASE_URL, new String[] {}, "")
                        .setStatusCode(403)
                        .setBodyValidationParams(bodyParams)
                        .build())
                .doPost(vr)
                .run();
    }

    @Test
    void validateRevalidation_commitUpdatedAfterError() {
        Commit c1 = createStandardUsercommit(USER_NEWBIE, USER_WIZARD);

        ValidationRequest vr = createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample", Arrays.asList(c1));

        Map<String, Object> bodyParams = new HashMap<>(FAIL_SINGLE_ERR_BODY_PARAMS);
        bodyParams.put("commits." + c1.getHash() + ".errors[0].code", APIStatusCode.ERROR_AUTHOR.getValue());

        EndpointTestCase testCase = TestCaseHelper
                .prepareTestCase(ECA_BASE_URL, new String[] {}, "")
                .setStatusCode(403)
                .setBodyValidationParams(bodyParams)
                .build();

        EndpointTestBuilder.from(testCase).doPost(vr).run();

        // simulate fixed ECA by updating author and using same hash
        c1 = Commit
                .builder()
                .setAuthor(USER_WIZARD)
                .setCommitter(USER_WIZARD)
                .setBody(String.format("Signed-off-by: %s <%s>", USER_WIZARD.getName(), USER_WIZARD.getMail()))
                .setHash(c1.getHash())
                .setSubject("All of the things")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();

        vr = createGitHubRequest(false, "http://www.github.com/eclipsefdn/sample", Arrays.asList(c1));

        bodyParams = new HashMap<>(SUCCESS_BODY_PARAMS);
        bodyParams.put("commits." + c1.getHash() + ".messages[0].code", APIStatusCode.SUCCESS_DEFAULT.getValue());

        testCase = TestCaseHelper.prepareTestCase(ECA_BASE_URL, new String[] {}, "").setBodyValidationParams(bodyParams).build();

        EndpointTestBuilder.from(testCase).doPost(vr).run();
    }

    /*
     * USER LOOKUP TESTS
     */
    @Test
    void validateUserLookup_failure_anonymous() {
        EndpointTestBuilder.from(LOOKUP_ANONYMOUS_CASE).run();
    }

    // by username
    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = "email", value = "opearson@important.co") })
    void validateUserLookup_username_userNotFound() {
        EndpointTestBuilder.from(LOOKUP_USERNAME_NOT_FOUND_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = "email", value = "opearson@important.co") })
    void validateUserLookup_username_userNoECA() {
        EndpointTestBuilder.from(LOOKUP_USERNAME_FORBIDDEN_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = "email", value = "opearson@important.co") })
    void validateUserLookup_username_userSuccess() {
        EndpointTestBuilder.from(LOOKUP_USERNAME_SUCCESS_CASE).run();
    }

    // by email
    @Test
    @TestSecurity(user = "newbieAnon", roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = "email", value = "newbie@important.co") })
    void validateUserLookup_failure_nonCommitter() {
        // committer required for email lookup
        EndpointTestBuilder.from(LOOKUP_FORBIDDEN_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = "email", value = "opearson@important.co") })
    void validateUserLookup_userNotFound() {
        EndpointTestBuilder.from(LOOKUP_NOT_FOUND_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = "email", value = "opearson@important.co") })
    void validateUserLookup_userNoECA() {
        EndpointTestBuilder.from(LOOKUP_FORBIDDEN_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = "email", value = "opearson@important.co") })
    void validateUserLookup_userSuccess() {
        EndpointTestBuilder.from(LOOKUP_SUCCESS_CASE).run();
    }

    // legacy lookup cases using email param

    @Test
    @TestSecurity(user = "newbieAnon", roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = "email", value = "newbie@important.co") })
    void validateUserLookup_legacy_failure_nonCommitter() {
        EndpointTestBuilder.from(LOOKUP_FORBIDDEN_EMAIL_PARAM_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = "email", value = "opearson@important.co") })
    void validateUserLookup_legacy_userNotFound() {
        EndpointTestBuilder.from(LOOKUP_NOT_FOUND_EMAIL_PARAM_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = "email", value = "opearson@important.co") })
    void validateUserLookup_legacy_userNoECA() {
        EndpointTestBuilder.from(LOOKUP_FORBIDDEN_EMAIL_PARAM_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = "email", value = "opearson@important.co") })
    void validateUserLookup_legacy_userSuccess() {
        EndpointTestBuilder.from(LOOKUP_SUCCESS_EMAIL_PARAM_CASE).run();
    }

    // The default commit for most users. Used for most user tests
    private static Commit createStandardUsercommit(GitUser author, GitUser committer) {
        return Commit
                .builder()
                .setAuthor(author)
                .setCommitter(committer)
                .setBody(String.format("Signed-off-by: %s <%s>", author.getName(), author.getMail()))
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
    }

    // The default commit for bots. Used for most bot tests
    private static Commit createNoBodyCommit(GitUser author, GitUser committer) {
        return Commit
                .builder()
                .setAuthor(author)
                .setCommitter(committer)
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
    }

    private static ValidationRequest createGitHubRequest(boolean strictMode, String repoUrl, List<Commit> commits) {
        return ValidationRequest
                .builder()
                .setStrictMode(strictMode)
                .setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create(repoUrl))
                .setCommits(commits)
                .build();
    }

    private static ValidationRequest createGitLabRequest(boolean strictMode, String repoUrl, List<Commit> commits) {
        return ValidationRequest
                .builder()
                .setStrictMode(strictMode)
                .setProvider(ProviderType.GITLAB)
                .setRepoUrl(URI.create(repoUrl))
                .setCommits(commits)
                .build();
    }

    private static ValidationRequest createGerritRequest(boolean strictMode, String repoUrl, List<Commit> commits) {
        return ValidationRequest
                .builder()
                .setStrictMode(strictMode)
                .setProvider(ProviderType.GERRIT)
                .setRepoUrl(URI.create(repoUrl))
                .setCommits(commits)
                .build();
    }
}
