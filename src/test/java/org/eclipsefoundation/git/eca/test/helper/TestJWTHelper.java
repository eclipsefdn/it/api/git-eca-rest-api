package org.eclipsefoundation.git.eca.test.helper;

import org.eclipsefoundation.git.eca.api.models.GithubAccessToken;
import org.eclipsefoundation.git.eca.helper.JwtHelper;

import io.quarkus.cache.CacheResult;
import io.quarkus.test.Mock;
import jakarta.inject.Singleton;

@Singleton
@Mock
public class TestJWTHelper extends JwtHelper {

    @Override
    public String getGhBearerString(String installationId) {
        return "Bearer " + getGithubAccessToken(installationId).getToken();
    }

    @CacheResult(cacheName = "accesstoken")
    @Override
    public GithubAccessToken getGithubAccessToken(String installationId) {
        return ghApi.getNewAccessToken("Bearer " + generateJwt(), apiVersion, installationId);
    }

    @Override
    public String generateJwt() {
        return "jwt-test";
    }
}
