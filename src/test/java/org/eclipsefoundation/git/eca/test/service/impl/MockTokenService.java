/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.test.service.impl;

import jakarta.inject.Singleton;

import org.eclipsefoundation.efservices.services.DrupalTokenService;

import io.quarkus.test.Mock;

@Mock
@Singleton
public class MockTokenService implements DrupalTokenService {

    @Override
    public String getToken() {
        return "";
    }

}
