/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.test.api;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.git.eca.api.GithubAPI;
import org.eclipsefoundation.git.eca.api.models.GithubAccessToken;
import org.eclipsefoundation.git.eca.api.models.GithubApplicationInstallationData;
import org.eclipsefoundation.git.eca.api.models.GithubCommit;
import org.eclipsefoundation.git.eca.api.models.GithubCommit.CommitData;
import org.eclipsefoundation.git.eca.api.models.GithubCommit.GitCommitUser;
import org.eclipsefoundation.git.eca.api.models.GithubCommit.GithubCommitUser;
import org.eclipsefoundation.git.eca.api.models.GithubCommitStatusRequest;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.PullRequest;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;

@Mock
@RestClient
@ApplicationScoped
public class MockGithubAPI implements GithubAPI {

    Map<String, Map<Integer, List<GithubCommit>>> commits;
    Map<String, Map<String, String>> commitStatuses;

    public MockGithubAPI() {
        this.commitStatuses = new HashMap<>();
        this.commits = new HashMap<>();
        this.commits
                .put("eclipsefdn/sample",
                        Map
                                .of(42, Arrays
                                        .asList(GithubCommit
                                                .builder()
                                                .setSha("sha-1234")
                                                .setAuthor(GithubCommitUser.builder().setLogin("testuser").build())
                                                .setCommitter(GithubCommitUser.builder().setLogin("testuser").build())
                                                .setParents(Collections.emptyList())
                                                .setCommit(CommitData
                                                        .builder()
                                                        .setAuthor(GitCommitUser
                                                                .builder()
                                                                .setName("The Wizard")
                                                                .setEmail("code.wiz@important.co")
                                                                .build())
                                                        .setCommitter(GitCommitUser
                                                                .builder()
                                                                .setName("The Wizard")
                                                                .setEmail("code.wiz@important.co")
                                                                .build())
                                                        .build())
                                                .build())));
    }

    @Override
    public PullRequest getPullRequest(String bearer, String apiVersion, String org, String repo, int pullNumber) {
        return PullRequest.builder().build();
    }

    @Override
    public RestResponse<List<GithubCommit>> getCommits(String bearer, String apiVersion, String org, String repo, int pullNumber) {
        String repoFullName = org + '/' + repo;
        List<GithubCommit> results = commits.get(repoFullName).get(pullNumber);
        if (results == null || !results.isEmpty()) {
            return RestResponse.status(404);
        }
        return RestResponse.ok(results);
    }

    @Override
    public Response updateStatus(String bearer, String apiVersion, String org, String repo, String prHeadSha,
            GithubCommitStatusRequest commitStatusUpdate) {
        String repoFullName = org + '/' + repo;
        commitStatuses.computeIfAbsent(repoFullName, m -> new HashMap<>()).merge(prHeadSha, commitStatusUpdate.getState(), (k, v) -> v);
        return Response.ok().build();
    }

    @Override
    public RestResponse<List<GithubApplicationInstallationData>> getInstallations(BaseAPIParameters params, String bearer) {
        throw new UnsupportedOperationException("Unimplemented method 'getInstallations'");
    }

    @Override
    public GithubAccessToken getNewAccessToken(String bearer, String apiVersion, String installationId) {
        return GithubAccessToken.builder().setToken("gh-token-" + installationId).setExpiresAt(LocalDateTime.now().plusHours(1L)).build();
    }

    @Override
    public Response getInstallationRepositories(BaseAPIParameters params, String bearer) {
        throw new UnsupportedOperationException("Unimplemented method 'getInstallationRepositories'");
    }
}
