/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.test.api;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.efservices.api.ProfileAPI;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUserBuilder;
import org.eclipsefoundation.efservices.api.models.EfUserCountryBuilder;
import org.eclipsefoundation.efservices.api.models.EfUserEcaBuilder;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;

/**
 * Simple stub for accounts API. Allows for easy testing of users that don't really exist upstream, and so that we don't need a real auth
 * token for data.
 * 
 * @author Martin Lowe
 *
 */
@Mock
@RestClient
@ApplicationScoped
public class MockAccountsAPI implements ProfileAPI {

    private Map<String, EfUser> users;

    public MockAccountsAPI() {
        int id = 0;
        this.users = new HashMap<>();
        users
                .put("newbie@important.co",
                        EfUserBuilder
                                .builder()
                                .isCommitter(false)
                                .picture("")
                                .firstName("")
                                .lastName("")
                                .fullName("")
                                .publisherAgreements(Collections.emptyMap())
                                .twitterHandle("")
                                .jobTitle("")
                                .website("")
                                .country(EfUserCountryBuilder.builder().build())
                                .interests(Collections.emptyList())
                                .uid(Integer.toString(id++))
                                .mail("newbie@important.co")
                                .name("newbieAnon")
                                .eca(EfUserEcaBuilder.builder().build())
                                .build());
        users
                .put("slom@eclipse-foundation.org",
                        EfUserBuilder
                                .builder()
                                .isCommitter(false)
                                .picture("")
                                .firstName("")
                                .lastName("")
                                .fullName("")
                                .publisherAgreements(Collections.emptyMap())
                                .twitterHandle("")
                                .jobTitle("")
                                .website("")
                                .country(EfUserCountryBuilder.builder().build())
                                .interests(Collections.emptyList())
                                .uid(Integer.toString(id++))
                                .mail("slom@eclipse-foundation.org")
                                .name("barshall_blathers")
                                .eca(EfUserEcaBuilder.builder().canContributeSpecProject(true).signed(true).build())
                                .build());
        users
                .put("tester@eclipse-foundation.org",
                        EfUserBuilder
                                .builder()
                                .isCommitter(false)
                                .picture("")
                                .firstName("")
                                .lastName("")
                                .fullName("")
                                .publisherAgreements(Collections.emptyMap())
                                .twitterHandle("")
                                .jobTitle("")
                                .website("")
                                .country(EfUserCountryBuilder.builder().build())
                                .interests(Collections.emptyList())
                                .uid(Integer.toString(id++))
                                .mail("tester@eclipse-foundation.org")
                                .name("mctesterson")
                                .eca(EfUserEcaBuilder.builder().canContributeSpecProject(false).signed(true).build())
                                .build());
        users
                .put("code.wiz@important.co",
                        EfUserBuilder
                                .builder()
                                .isCommitter(true)
                                .picture("")
                                .firstName("")
                                .lastName("")
                                .fullName("")
                                .publisherAgreements(Collections.emptyMap())
                                .twitterHandle("")
                                .jobTitle("")
                                .website("")
                                .country(EfUserCountryBuilder.builder().build())
                                .interests(Collections.emptyList())
                                .uid(Integer.toString(id++))
                                .mail("code.wiz@important.co")
                                .name("da_wizz")
                                .eca(EfUserEcaBuilder.builder().canContributeSpecProject(true).signed(true).build())
                                .githubHandle("wiz_in_da_hub")
                                .build());
        users
                .put("grunt@important.co",
                        EfUserBuilder
                                .builder()
                                .isCommitter(true)
                                .picture("")
                                .firstName("")
                                .lastName("")
                                .fullName("")
                                .publisherAgreements(Collections.emptyMap())
                                .twitterHandle("")
                                .jobTitle("")
                                .website("")
                                .country(EfUserCountryBuilder.builder().build())
                                .interests(Collections.emptyList())
                                .uid(Integer.toString(id++))
                                .mail("grunt@important.co")
                                .name("grunter")
                                .eca(EfUserEcaBuilder.builder().canContributeSpecProject(false).signed(true).build())
                                .githubHandle("grunter2")
                                .build());
        users
                .put("paper.pusher@important.co",
                        EfUserBuilder
                                .builder()
                                .isCommitter(false)
                                .picture("")
                                .firstName("")
                                .lastName("")
                                .fullName("")
                                .publisherAgreements(Collections.emptyMap())
                                .twitterHandle("")
                                .jobTitle("")
                                .website("")
                                .country(EfUserCountryBuilder.builder().build())
                                .interests(Collections.emptyList())
                                .uid(Integer.toString(id++))
                                .mail("paper.pusher@important.co")
                                .name("sumAnalyst")
                                .eca(EfUserEcaBuilder.builder().canContributeSpecProject(false).signed(true).build())
                                .build());
        users
                .put("opearson@important.co",
                        EfUserBuilder
                                .builder()
                                .isCommitter(true)
                                .picture("")
                                .firstName("")
                                .lastName("")
                                .fullName("")
                                .publisherAgreements(Collections.emptyMap())
                                .twitterHandle("")
                                .jobTitle("")
                                .website("")
                                .country(EfUserCountryBuilder.builder().build())
                                .interests(Collections.emptyList())
                                .uid(Integer.toString(id++))
                                .mail("opearson@important.co")
                                .name("opearson")
                                .eca(EfUserEcaBuilder.builder().canContributeSpecProject(false).signed(true).build())
                                .githubHandle("opearson")
                                .build());
    }

    @Override
    public EfUser getUserByEfUsername(String token, String uname) {
        return users.values().stream().filter(usernamePredicate(uname)).findFirst().orElseGet(() -> null);
    }

    @Override
    public EfUser getUserByGithubHandle(String token, String ghHandle) {
        return users
                .values()
                .stream()
                .filter(u -> u.githubHandle() != null && u.githubHandle().equals(ghHandle))
                .findFirst()
                .orElseGet(() -> null);
    }

    @Override
    public List<EfUser> getUsers(String token, UserSearchParams params) {
        return users
                .values()
                .stream()
                .filter(usernamePredicate(params.name))
                .filter(u -> params.mail == null || u.mail().equals(params.mail))
                .filter(u -> params.uid == null || u.uid().equals(params.uid))
                .toList();
    }

    private Predicate<EfUser> usernamePredicate(String target) {
        return u -> target == null || u.name().equals(target);
    }
}
