/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.test.namespaces;

public final class SchemaNamespaceHelper {
	public static final String BASE_SCHEMAS_PATH = "schemas/";
	public static final String BASE_SCHEMAS_PATH_SUFFIX = "-schema.json";

	public static final String VALIDATION_REQUEST_SCHEMA_PATH = BASE_SCHEMAS_PATH + "validation-request"
			+ BASE_SCHEMAS_PATH_SUFFIX;
	public static final String VALIDATION_RESPONSE_SCHEMA_PATH = BASE_SCHEMAS_PATH + "validation-response"
			+ BASE_SCHEMAS_PATH_SUFFIX;

	public static final String PRIVATE_PROJECT_EVENT_SCHEMA_PATH = BASE_SCHEMAS_PATH + "private-project-event"
			+ BASE_SCHEMAS_PATH_SUFFIX;
	public static final String PRIVATE_PROJECT_EVENTS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "private-project-events"
			+ BASE_SCHEMAS_PATH_SUFFIX;

	public static final String COMMIT_VALIDATION_STATUS_SCHEMA = BASE_SCHEMAS_PATH + "commit-validation-status"
			+ BASE_SCHEMAS_PATH_SUFFIX;
	public static final String COMMIT_VALIDATION_STATUSES_SCHEMA = BASE_SCHEMAS_PATH + "commit-validation-statuses"
			+ BASE_SCHEMAS_PATH_SUFFIX;

	public static final String ERROR_SCHEMA_PATH = BASE_SCHEMAS_PATH + "error" + BASE_SCHEMAS_PATH_SUFFIX;
}
