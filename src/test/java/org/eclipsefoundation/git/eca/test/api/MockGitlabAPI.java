/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.test.api;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.git.eca.api.GitlabAPI;
import org.eclipsefoundation.git.eca.api.models.GitlabProjectResponse;
import org.eclipsefoundation.git.eca.api.models.GitlabProjectResponse.ForkedProject;
import org.eclipsefoundation.git.eca.api.models.GitlabUserResponse;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;

@Mock
@RestClient
@ApplicationScoped
public class MockGitlabAPI implements GitlabAPI {

    private List<GitlabProjectResponse> projects;
    private List<GitlabUserResponse> users;

    public MockGitlabAPI() {
        projects = new ArrayList<>();
        projects.addAll(Arrays.asList(
                GitlabProjectResponse.builder()
                        .setId(69)
                        .setCreatorId(1)
                        .setPathWithNamespace("namespace/path")
                        .setCreatedAt(ZonedDateTime.now())
                        .build(),
                GitlabProjectResponse.builder()
                        .setId(42)
                        .setCreatorId(55)
                        .setForkedFromProject(ForkedProject.builder().setId(41).build())
                        .setPathWithNamespace("namespace/path")
                        .setCreatedAt(ZonedDateTime.now())
                        .build(),
                GitlabProjectResponse.builder()
                        .setId(95)
                        .setCreatorId(33)
                        .setForkedFromProject(ForkedProject.builder().setId(69).build())
                        .setPathWithNamespace("namespace/path")
                        .setCreatedAt(ZonedDateTime.now())
                        .build()));

        users = new ArrayList<>();
        users.addAll(Arrays.asList(
                GitlabUserResponse.builder()
                        .setId(1)
                        .setUsername("admin")
                        .build(),
                GitlabUserResponse.builder()
                        .setId(55)
                        .setUsername("testaccount")
                        .build(),
                GitlabUserResponse.builder()
                        .setId(33)
                        .setUsername("fakeuser")
                        .build()));
    }

    @Override
    public GitlabProjectResponse getProjectInfo(String privateToken, int projectId) {
        return projects.stream().filter(p -> p.getId() == projectId).findFirst().orElseGet(null);
    }

    @Override
    public RestResponse<List<GitlabProjectResponse>> getPrivateProjects(BaseAPIParameters baseParams, String privateToken, String visibility,
            Integer perPage) {
        return RestResponse.ok();
    }

    @Override
    public GitlabUserResponse getUserInfo(String privateToken, int userId) {
        return users.stream().filter(p -> p.getId() == userId).findFirst().orElseGet(null);
    }
}
