/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.helper;

import jakarta.inject.Inject;

import org.eclipsefoundation.git.eca.namespace.HCaptchaErrorCodes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Tests for validating success cases for the
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class CaptchaHelperTest {
    public static final String SUCCESS_CASE_RESPONSE_VALUE = "20000000-aaaa-bbbb-cccc-000000000002";

    @Inject
    CaptchaHelper helper;

    @Test
    void validateCaptchaResponse_success() {
        Assertions.assertTrue(helper.validateCaptchaResponse(SUCCESS_CASE_RESPONSE_VALUE).isEmpty());
    }

    @Test
    void validateCaptchaResponse_failure_badResponse() {
        Assertions.assertTrue(helper.validateCaptchaResponse("sample bad response").contains(HCaptchaErrorCodes.INVALID_INPUT_RESPONSE));
    }

    @Test
    void validateCaptchaResponse_failure_emptyResponse() {
        Assertions.assertTrue(helper.validateCaptchaResponse("").contains(HCaptchaErrorCodes.MISSING_INPUT_RESPONSE));
    }
}
