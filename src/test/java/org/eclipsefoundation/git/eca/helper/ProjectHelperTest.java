package org.eclipsefoundation.git.eca.helper;

import java.util.List;

import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.git.eca.namespace.ProviderType;
import org.eclipsefoundation.git.eca.test.api.MockProjectsAPI;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;

@QuarkusTest
class ProjectHelperTest {

    @Inject
    ProjectHelper helper;

    /**
     * Gitlab provider tests
     */
    @Test
    void retrieveProjectsForRepoURL_success_gitlabOrg() {
        List<Project> results = helper
                .retrieveProjectsForRepoURL("http://www.gitlab.eclipse.org/eclipse/dash-second/sample-repo", ProviderType.GITLAB);
        Assertions.assertEquals(1, results.size());
        Assertions.assertEquals("sample.proto", results.get(0).projectId());
    }

    @Test
    void retrieveProjectsForRepoURL_success_gitlabRepo() {
        List<Project> results = helper
                .retrieveProjectsForRepoURL("http://www.gitlab.eclipse.org/eclipsefdn/sample/repo", ProviderType.GITLAB);
        Assertions.assertEquals(1, results.size());
        Assertions.assertEquals("spec.proj", results.get(0).projectId());
    }

    @Test
    void retrieveProjectsForRepoURL_failure_gitlab_noMatch() {
        List<Project> results = helper
                .retrieveProjectsForRepoURL("http://www.gitlab.eclipse.org/eclipsefdn/something-that-doesnt-exist", ProviderType.GITLAB);
        Assertions.assertEquals(0, results.size());
    }

    @Test
    void retrieveProjectsForRepoURL_failure_gitlab_onlyChecksGLRepos() {
        List<Project> results = helper.retrieveProjectsForRepoURL("http://www.github.com/eclipsefdn/tck-proto", ProviderType.GITLAB);
        Assertions.assertEquals(0, results.size());
    }

    /**
     * Github provider
     */
    @Test
    void retrieveProjectsForRepoURL_success_githubOrg() {
        List<Project> results = helper.retrieveProjectsForRepoURL("http://www.github.com/eclipsefdn-tck/dash-second", ProviderType.GITHUB);
        Assertions.assertEquals(1, results.size());
        Assertions.assertEquals("spec.proj", results.get(0).projectId());
    }

    @Test
    void retrieveProjectsForRepoURL_success_githubRepo() {
        List<Project> results = helper.retrieveProjectsForRepoURL("http://www.github.com/eclipsefdn/tck-proto", ProviderType.GITHUB);
        Assertions.assertEquals(1, results.size());
        Assertions.assertEquals("spec.proj", results.get(0).projectId());
    }

    @Test
    void retrieveProjectsForRepoURL_failure_github_noMatch() {
        List<Project> results = helper
                .retrieveProjectsForRepoURL("http://www.github.com/eclipsefdn/something-that-doesnt-exist", ProviderType.GITHUB);
        Assertions.assertEquals(0, results.size());
    }

    @Test
    void retrieveProjectsForRepoURL_failure_github_onlyChecksGHRepos() {
        List<Project> results = helper
                .retrieveProjectsForRepoURL("http://www.gitlab.eclipse.org/eclipsefdn/sample/repo", ProviderType.GITHUB);
        Assertions.assertEquals(0, results.size());
    }

    @Test
    void retrieveProjectsForRepoURL_success_websiteRepo() {
        List<Project> results = helper.retrieveProjectsForRepoURL(MockProjectsAPI.WEBSITE_REPO_URL, ProviderType.GITHUB);
        Assertions.assertEquals(1, results.size());
        Assertions.assertEquals("sample.proj", results.get(0).projectId());
    }
}
