/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.helper;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.GitUser;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.namespace.ProviderType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Tests related to the {@linkplain CommitHelper} class.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class CommitHelperTest {

    // represents a known good commit before the start of each test
    GitUser testUser;
    Commit.Builder baseCommit;

    @BeforeEach
    void setup() {
        // basic good user
        testUser = GitUser.builder().setMail("test.user@eclipse-foundation.org").setName("Tester McTesterson").build();

        // basic known good commit
        baseCommit = Commit
                .builder()
                .setBody(String.format("Sample body content\n\nSigned-off-by: %s <%s>", testUser.getName(), testUser.getMail()))
                .setHash("abc123f")
                .setHead(false)
                .setParents(new ArrayList<>())
                .setSubject("Testing CommitHelper class #1337")
                .setAuthor(testUser)
                .setCommitter(testUser);
    }

    @Test
    void validateCommitKnownGood() {
        Assertions.assertTrue(CommitHelper.validateCommit(baseCommit.build()), "Expected basic commit to pass validation");
    }

    @Test
    void validateCommitNullCommit() {
        Assertions.assertFalse(CommitHelper.validateCommit(null), "Expected null commit to fail validation");
    }

    @Test
    void validateCommitNoAuthorMail() {
        baseCommit.setAuthor(GitUser.builder().setName("Some Name").build());
        Assertions
                .assertFalse(CommitHelper.validateCommit(baseCommit.build()),
                        "Expected basic commit to fail validation w/ no author mail address");
    }

    @Test
    void validateCommitNoCommitterMail() {
        baseCommit.setCommitter(GitUser.builder().setName("Some Name").build());
        Assertions
                .assertFalse(CommitHelper.validateCommit(baseCommit.build()),
                        "Expected basic commit to fail validation w/ no committer mail address");
    }

    @Test
    void validateCommitNoBody() {
        baseCommit.setBody(null);
        Assertions.assertTrue(CommitHelper.validateCommit(baseCommit.build()), "Expected basic commit to pass validation w/ no body");
    }

    @Test
    void validateCommitNoParents() {
        baseCommit.setParents(new ArrayList<>());
        Assertions.assertTrue(CommitHelper.validateCommit(baseCommit.build()), "Expected basic commit to pass validation w/ no parents");
    }

    @Test
    void validateCommitNoSubject() {
        baseCommit.setSubject(null);
        Assertions.assertTrue(CommitHelper.validateCommit(baseCommit.build()), "Expected basic commit to pass validation w/ no subject");
    }

    @Test
    void generateRequestHash_reproducible() {
        ValidationRequest vr = generateBaseRequest();
        String fingerprint = CommitHelper.generateRequestHash(vr);
        // if pushing the same set of commits without change the fingerprint won't change
        Assertions.assertEquals(fingerprint, CommitHelper.generateRequestHash(vr));
    }

    @Test
    void generateRequestHash_changesWithRepoURL() {
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit
                .builder()
                .setAuthor(g1)
                .setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>")
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things")
                .setParents(Collections.emptyList())
                .build();
        commits.add(c1);

        // generate initial fingerprint
        ValidationRequest vr = ValidationRequest
                .builder()
                .setStrictMode(false)
                .setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample"))
                .setCommits(commits)
                .build();
        String fingerprint = CommitHelper.generateRequestHash(vr);
        // generate request with different repo url
        vr = ValidationRequest
                .builder()
                .setStrictMode(false)
                .setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/other-sample"))
                .setCommits(commits)
                .build();
        // fingerprint should change based on repo URL to reduce risk of collision
        Assertions.assertNotEquals(fingerprint, CommitHelper.generateRequestHash(vr));
    }

    @Test
    void generateRequestHash_changesWithNewCommits() {
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();
        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit
                .builder()
                .setAuthor(g1)
                .setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>")
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things")
                .setParents(Collections.emptyList())
                .build();
        commits.add(c1);

        // generate initial fingerprint
        ValidationRequest vr = ValidationRequest
                .builder()
                .setStrictMode(false)
                .setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample"))
                .setCommits(commits)
                .build();
        String fingerprint = CommitHelper.generateRequestHash(vr);
        Commit c2 = Commit
                .builder()
                .setAuthor(g1)
                .setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>")
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things")
                .setParents(Collections.emptyList())
                .build();
        commits.add(c2);
        // generate request with different repo url
        vr = ValidationRequest
                .builder()
                .setStrictMode(false)
                .setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/other-sample"))
                .setCommits(commits)
                .build();
        // each commit added should modify the fingerprint at least slightly
        Assertions.assertNotEquals(fingerprint, CommitHelper.generateRequestHash(vr));
    }

    /**
     * Used when a random validationRequest is needed and will not need to be recreated/modified. Base request should
     * register as a commit for the test `sample.proj` project.
     * 
     * @return random basic validation request.
     */
    private ValidationRequest generateBaseRequest() {
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();
        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit
                .builder()
                .setAuthor(g1)
                .setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>")
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things")
                .setParents(Collections.emptyList())
                .build();
        commits.add(c1);
        return ValidationRequest
                .builder()
                .setStrictMode(false)
                .setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample"))
                .setCommits(commits)
                .build();
    }
}
