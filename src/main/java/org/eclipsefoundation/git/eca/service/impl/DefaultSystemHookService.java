/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.service.impl;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.model.CacheWrapper;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.git.eca.api.GitlabAPI;
import org.eclipsefoundation.git.eca.api.models.GitlabProjectResponse;
import org.eclipsefoundation.git.eca.api.models.GitlabUserResponse;
import org.eclipsefoundation.git.eca.api.models.SystemHook;
import org.eclipsefoundation.git.eca.dto.PrivateProjectEvent;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.service.SystemHookService;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.control.ActivateRequestContext;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

@ApplicationScoped
@ActivateRequestContext
public class DefaultSystemHookService implements SystemHookService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSystemHookService.class);

    @ConfigProperty(name = "eclipse.gitlab.access-token")
    String apiToken;

    @ConfigProperty(name = "eclipse.system-hook.pool-size")
    Integer poolSize;
    @ConfigProperty(name = "eclipse.system-hook.delay-in-ms", defaultValue = "300")
    Integer schedulingDelay;

    @Inject
    @RestClient
    GitlabAPI api;

    @Inject
    CachingService cache;

    @Inject
    PersistenceDao dao;

    @Inject
    FilterService filters;

    ScheduledExecutorService ses;

    @PostConstruct
    public void init() {
        ses = Executors.newScheduledThreadPool(poolSize);
    }

    @PreDestroy
    public void destroy() {
        ses.shutdownNow();
    }

    @Override
    public void processProjectCreateHook(RequestWrapper wrapper, SystemHook hook) {
        if (hook != null) {
            ses.schedule(() -> trackPrivateProjectCreation(wrapper, hook), schedulingDelay, TimeUnit.MILLISECONDS);
        }
    }

    @Override
    public void processProjectDeleteHook(RequestWrapper wrapper, SystemHook hook) {
        if (hook != null) {
            ses.execute(() -> trackPrivateProjectDeletion(wrapper, hook));
        }
    }

    @Override
    public void processProjectRenameHook(RequestWrapper wrapper, SystemHook hook) {
        if (hook != null) {
            ses.schedule(() -> trackPrivateProjectRenaming(wrapper, hook), schedulingDelay, TimeUnit.MILLISECONDS);
        }
    }

    /**
     * Gathers all relevant data related to the created project and persists the information into a database
     * 
     * @param wrapper the request wrapper containing all uri, ip, and query params
     * @param hook the incoming system hook
     */
    @Transactional
    void trackPrivateProjectCreation(RequestWrapper wrapper, SystemHook hook) {
        try {
            LOGGER.debug("Tracking creation of project: [id: {}, path: {}]", hook.getProjectId(), hook.getPathWithNamespace());

            CacheWrapper<GitlabProjectResponse> response = cache
                    .get(Integer.toString(hook.getProjectId()), new MultivaluedHashMap<>(), GitlabProjectResponse.class,
                            () -> api.getProjectInfo(apiToken, hook.getProjectId()));

            if (response.data().isPresent()) {
                PrivateProjectEvent dto = mapToDto(hook, response.data().get());
                dao.add(new RDBMSQuery<>(wrapper, filters.get(PrivateProjectEvent.class)), Arrays.asList(dto));
            } else {
                LOGGER.error("No info for project: [id: {}, path: {}]", hook.getProjectId(), hook.getPathWithNamespace());
            }

        } catch (Exception e) {
            LOGGER.error("Error fetching data relevant project data from GL for project: {}", hook.getProjectId(), e);
        }
    }

    /**
     * Retrieves the event record for the project that has been deleted. Adds the deletionDate field and updates the DB
     * record.
     * 
     * @param wrapper the request wrapper containing all uri, ip, and query params
     * @param hook the incoming system hook
     */
    @Transactional
    void trackPrivateProjectDeletion(RequestWrapper wrapper, SystemHook hook) {
        try {
            LOGGER.debug("Tracking deletion of project: [id: {}, path: {}]", hook.getProjectId(), hook.getPathWithNamespace());

            MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
            params.add(GitEcaParameterNames.PROJECT_ID.getName(), Integer.toString(hook.getProjectId()));
            params.add(GitEcaParameterNames.PROJECT_PATH.getName(), hook.getPathWithNamespace());

            List<PrivateProjectEvent> results = dao.get(new RDBMSQuery<>(wrapper, filters.get(PrivateProjectEvent.class), params));

            if (results.isEmpty()) {
                LOGGER.error("No results for project event: [id: {}, path: {}]", hook.getProjectId(), hook.getPathWithNamespace());
            } else {
                results.get(0).setDeletionDate(hook.getUpdatedAt().toLocalDateTime());
                dao.add(new RDBMSQuery<>(wrapper, filters.get(PrivateProjectEvent.class)), results);
            }
        } catch (Exception e) {
            LOGGER.error(String.format("Error updating project: [id: %s, path: %s]", hook.getProjectId(), hook.getPathWithNamespace()), e);
        }
    }

    /**
     * Retrieves the event record for the project that has been renamed. Updates the projectPath field and creates a new DB
     * record.
     * 
     * @param wrapper the request wrapper containing all uri, ip, and query params
     * @param hook the incoming system hook
     */
    @Transactional
    void trackPrivateProjectRenaming(RequestWrapper wrapper, SystemHook hook) {
        try {
            LOGGER.debug("Tracking renaming of project: [id: {}, path: {}]", hook.getProjectId(), hook.getOldPathWithNamespace());

            MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
            params.add(GitEcaParameterNames.PROJECT_ID.getName(), Integer.toString(hook.getProjectId()));
            params.add(GitEcaParameterNames.PROJECT_PATH.getName(), hook.getOldPathWithNamespace());

            List<PrivateProjectEvent> results = dao.get(new RDBMSQuery<>(wrapper, filters.get(PrivateProjectEvent.class), params));

            if (results.isEmpty()) {
                LOGGER.error("No results for project event: [id: {}, path: {}]", hook.getProjectId(), hook.getOldPathWithNamespace());
            } else {
                // Create a new event and track in the DB
                PrivateProjectEvent event = new PrivateProjectEvent(results.get(0).getCompositeId().getUserId(),
                        results.get(0).getCompositeId().getProjectId(), hook.getPathWithNamespace());

                event.setEFUsername(fetchUserName(event.getCompositeId().getUserId()));
                event.setCreationDate(LocalDateTime.now());
                event.setParentProject(results.get(0).getParentProject());

                dao.add(new RDBMSQuery<>(wrapper, filters.get(PrivateProjectEvent.class)), Arrays.asList(event));
            }
        } catch (Exception e) {
            LOGGER
                    .error(String.format("Error updating project: [id: %s, path: %s]", hook.getProjectId(), hook.getOldPathWithNamespace()),
                            e);
        }
    }

    /**
     * Takes the received hook and Gitlab api response body and maps the values to a PrivateProjectEvent dto.
     * 
     * @param hookthe received system hook
     * @param projectInfo The gitlab project data
     * @return A PrivateProjectEvent object
     */
    private PrivateProjectEvent mapToDto(SystemHook hook, GitlabProjectResponse projectInfo) {

        PrivateProjectEvent eventDto = new PrivateProjectEvent(projectInfo.getCreatorId(), hook.getProjectId(),
                hook.getPathWithNamespace());

        eventDto.setEFUsername(fetchUserName(projectInfo.getCreatorId()));
        eventDto.setCreationDate(hook.getCreatedAt().toLocalDateTime());

        if (projectInfo.getForkedFromProject() != null) {
            eventDto.setParentProject(projectInfo.getForkedFromProject().getId());
        }

        return eventDto;
    }

    /**
     * fetches a user's name using the given user ID.
     * 
     * @param userId the desried user's id
     * @return the username or null
     */
    private String fetchUserName(Integer userId) {
        Optional<GitlabUserResponse> response = cache
                .get(Integer.toString(userId), new MultivaluedHashMap<>(), GitlabUserResponse.class,
                        () -> api.getUserInfo(apiToken, userId))
                .data();
        return response.isPresent() ? response.get().getUsername() : null;
    }
}
