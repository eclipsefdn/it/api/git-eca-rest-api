/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.service;

import java.util.List;
import java.util.Optional;

import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.PullRequest;
import org.eclipsefoundation.git.eca.dto.GithubApplicationInstallation;

/**
 * Service for interacting with the Github API with caching for performance.
 * 
 * @author Martin Lowe
 *
 */
public interface GithubApplicationService {

    /**
     * Retrieve safe list of all installations that are managed by this instance of the Eclispe ECA app.
     * 
     * @return list containing installation records for the current application
     */
    List<GithubApplicationInstallation> getManagedInstallations();

    /**
     * Retrieves the installation ID for the ECA app on the given org or repo if it exists.
     * 
     * @param org the name of organization to retrieve an installation ID for
     * @param repo the name of repo to retrieve an installation ID for
     * @return the numeric installation ID if it exists, or null
     */
    String getInstallationForRepo(String org, String repo);

    /**
     * Retrieves a pull request given the repo, pull request, and associated installation to action the fetch.
     * 
     * @param installationId installation ID to use when creating access tokens to query GH API
     * @param repoFullName Github organization that owns the repo that holds the PR being validated
     * @param repoName name of the repository within the organization that owns the PR
     * @param pullRequest the pull request numeric ID
     * @return the pull request if it exists, otherwise empty
     */
    Optional<PullRequest> getPullRequest(String installationId, String org, String repoName, Integer pullRequest);
}
