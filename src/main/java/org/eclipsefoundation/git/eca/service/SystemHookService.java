/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.service;

import org.eclipsefoundation.git.eca.api.models.SystemHook;
import org.eclipsefoundation.http.model.RequestWrapper;

/**
 * Processes the various system hooks received.
 */
public interface SystemHookService {

    /**
     * Processes a project_create hook
     * 
     * @param wrapper
     * @param hook
     */
    void processProjectCreateHook(RequestWrapper wrapper, SystemHook hook);

    /**
     * Processes a project_destroy hook
     * 
     * @param wrapper
     * @param hook
     */
    void processProjectDeleteHook(RequestWrapper wrapper, SystemHook hook);

    /**
     * Processes a project_rename hook
     * 
     * @param wrapper
     * @param hook
     */
    void processProjectRenameHook(RequestWrapper wrapper, SystemHook hook);
}
