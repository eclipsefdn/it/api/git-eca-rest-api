/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.service;

import java.time.LocalDate;
import java.util.List;

import org.eclipsefoundation.git.eca.model.PrivateProjectData;
import org.eclipsefoundation.http.model.RequestWrapper;

/**
 * This interface provides the means to get list of desired entities from the DB
 * using desired filters.
 */
public interface ReportsService {

    /**
     * Fetches a filtered list of PrivateProjectEvents from the DB using the desired
     * parameters. Can be filtered by active/deleted status, and by date range
     * 
     * @param wrap   The request wrapper
     * @param status The current project status
     * @param since  The starting range
     * @param until  The ending range
     * @return A List of PrivateProjectData entities
     */
    public List<PrivateProjectData> getPrivateProjectEvents(RequestWrapper wrap, String status, LocalDate since,
            LocalDate until);
}
