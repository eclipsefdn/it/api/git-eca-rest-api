/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentHashMap.KeySetView;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.git.eca.dto.CommitValidationMessage;
import org.eclipsefoundation.git.eca.dto.CommitValidationStatus;
import org.eclipsefoundation.git.eca.dto.CommitValidationStatusGrouping;
import org.eclipsefoundation.git.eca.helper.CommitHelper;
import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.CommitStatus;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.model.ValidationResponse;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.service.ValidationStatusService;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.utils.helper.DateTimeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * 
 */
@ApplicationScoped
public class DefaultValidationStatusService implements ValidationStatusService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultValidationStatusService.class);

    @Inject
    PersistenceDao dao;
    @Inject
    FilterService filters;

    @Override
    public List<CommitValidationStatus> getHistoricValidationStatus(RequestWrapper wrapper, String fingerprint) {
        if (StringUtils.isAllBlank(fingerprint)) {
            return Collections.emptyList();
        }
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(GitEcaParameterNames.FINGERPRINT_RAW, fingerprint);
        RDBMSQuery<CommitValidationStatus> q = new RDBMSQuery<>(wrapper, filters.get(CommitValidationStatus.class), params);
        // set use limit to false to collect all data in one request
        q.setUseLimit(false);
        return deduplicateErrorMessages(dao.get(q));
    }

    @Override
    public List<CommitValidationStatus> getHistoricValidationStatusByShas(RequestWrapper wrapper, List<String> shas) {
        if (shas == null || shas.isEmpty()) {
            return Collections.emptyList();
        }
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.put(GitEcaParameterNames.SHAS_RAW, shas);
        RDBMSQuery<CommitValidationStatus> q = new RDBMSQuery<>(wrapper, filters.get(CommitValidationStatus.class), params);
        // set use limit to false to collect all data in one request
        q.setUseLimit(false);
        return deduplicateErrorMessages(dao.get(q));
    }

    @Override
    public List<CommitValidationStatus> getRequestCommitValidationStatus(RequestWrapper wrapper, ValidationRequest req, String projectId) {
        RDBMSQuery<CommitValidationStatus> q = new RDBMSQuery<>(wrapper, filters.get(CommitValidationStatus.class),
                CommitHelper.getCommitParams(req, projectId));
        // set use limit to false to collect all data in one request
        q.setUseLimit(false);
        return deduplicateErrorMessages(dao.get(q));
    }

    @Override
    public void updateCommitValidationStatus(RequestWrapper wrapper, ValidationResponse r, ValidationRequest req,
            List<CommitValidationStatus> statuses, Project p) {
        // iterate over commit responses, and update statuses in DB
        List<CommitValidationStatus> updatedStatuses = r
                .getCommits()
                .entrySet()
                .stream()
                .filter(e -> !ValidationResponse.NIL_HASH_PLACEHOLDER.equalsIgnoreCase(e.getKey()))
                .map(e -> recordUpdatedValidationStatus(req, statuses, wrapper, p, e))
                .toList();
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Persisting statuses for hashes: {}", updatedStatuses.stream().map(CommitValidationStatus::getCommitHash));
        }

        String fingerprint = CommitHelper.generateRequestHash(req);
        // update the base commit status and messages
        dao.add(new RDBMSQuery<>(wrapper, filters.get(CommitValidationStatus.class)), updatedStatuses);
        dao
                .add(new RDBMSQuery<>(wrapper, filters.get(CommitValidationStatusGrouping.class)),
                        updatedStatuses.stream().map(s -> new CommitValidationStatusGrouping(fingerprint, s)).toList());
    }

    /**
     * Records new or updated validation status for passed commit entry. Checks existing records and updates where appropriate, otherwise
     * creating new entires.
     * 
     * @param req the complete validation request
     * @param existingStatuses the statuses that may exist for the current request
     * @param p the Eclipse project for the current request if it exists.
     * @param e the current commit that is being updated
     * @return the new or updated commit status
     */
    private CommitValidationStatus recordUpdatedValidationStatus(ValidationRequest req, List<CommitValidationStatus> statuses,
            RequestWrapper wrap, Project p, Entry<String, CommitStatus> e) {
        // get the commit for current status
        Optional<Commit> commit = req.getCommits().stream().filter(c -> e.getKey().equals(c.getHash())).findFirst();
        if (commit.isEmpty()) {
            // this should always have a match (response commits are built from request commits)
            LOGGER.error("Could not find request commit associated with commit messages for commit hash '{}'", e.getKey());
            return null;
        }
        Commit c = commit.get();

        // update the status if present, otherwise make new one.
        CommitValidationStatus base = getStatusForHash(c, req, statuses, wrap, p);
        base.setLastModified(DateTimeHelper.now());

        // if there are errors, update validation messages
        if (!e.getValue().getErrors().isEmpty() || (base.getErrors() != null && !base.getErrors().isEmpty())) {
            // generate new errors, looking for errors not found in current list
            List<CommitValidationMessage> currentErrors = base.getErrors() != null ? base.getErrors() : new ArrayList<>();
            List<CommitValidationMessage> newErrors = e
                    .getValue()
                    .getErrors()
                    .stream()
                    .filter(err -> currentErrors.stream().noneMatch(ce -> ce.getStatusCode() == err.getCode().getValue()))
                    .map(err -> {
                        CommitValidationMessage m = new CommitValidationMessage();
                        m.setAuthorEmail(c.getAuthor().getMail());
                        m.setCommitterEmail(c.getCommitter().getMail());
                        m.setStatusCode(err.getCode().getValue());
                        // TODO add a checked way to set this
                        m.setEclipseId(null);
                        // if a committer status, provide the committer external ID, otherwise pass author external ID
                        m
                                .setProviderId(err.getCode().isCommitterError() ? c.getCommitter().getExternalId()
                                                : c.getAuthor().getExternalId());
                        m.setCommit(base);
                        return m;
                    })
                    .toList();
            LOGGER.debug("Encountered {} new errors for commit with hash '{}'", newErrors.size(), e.getKey());
            currentErrors.addAll(newErrors);
            // remove errors that weren't encountered on this run
            currentErrors
                    .removeIf(err -> e.getValue().getErrors().isEmpty()
                            || e.getValue().getErrors().stream().noneMatch(msg -> msg.getCode().getValue() == err.getStatusCode()));
            // iss #160 - remove errors that are duplicated based on commit hash + error code
            currentErrors.removeIf(distinctByKey(err -> err.getCommit().getCommitHash() + err.getStatusCode()));

            LOGGER.trace("Encountered {} errors: {}", currentErrors.size(), currentErrors);
            base.setErrors(currentErrors);
        }
        return base;
    }

    /**
     * Retrieves a status for a hash, first checking the existing list fetched in bulk at the start of the request, and then attempting to
     * fetch it fresh, before finally creating a new entry.
     * 
     * @param c commit being processed
     * @param req the original request for validation
     * @param statuses the list of original statuses fetched at the start of processing
     * @param wrap the current request
     * @param p project associated with the request if it exists.
     * @return the commit validation status for the passed hash.
     */
    private CommitValidationStatus getStatusForHash(Commit c, ValidationRequest req, List<CommitValidationStatus> statuses,
            RequestWrapper wrap, Project p) {
        Optional<CommitValidationStatus> status = statuses.stream().filter(s -> c.getHash().equals(s.getCommitHash())).findFirst();
        if (!status.isEmpty()) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER
                        .trace("Found existing commit status for commit '{}' in repository {}, {} existing errors", c.getHash(),
                                req.getRepoUrl(), status.get().getErrors().size());
            }
            return status.get();
        } else {
            // lookup the existing status to prevent issues with near parallel requests/duplicate commit data
            MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
            params.add(GitEcaParameterNames.SHAS_RAW, c.getHash());
            params.add(GitEcaParameterNames.REPO_URL_RAW, req.getRepoUrl().toString());
            List<CommitValidationStatus> existingStatus = dao
                    .get(new RDBMSQuery<>(wrap, filters.get(CommitValidationStatus.class), params));
            if (existingStatus != null && !existingStatus.isEmpty()) {
                return existingStatus.get(0);
            }

            // create the missing validation status and return it
            CommitValidationStatus base = new CommitValidationStatus();
            base.setProject(CommitHelper.getProjectId(p));
            base.setCommitHash(c.getHash());
            base.setUserMail(c.getAuthor().getMail());
            base.setProvider(req.getProvider());
            base.setRepoUrl(req.getRepoUrl().toString());
            base.setCreationDate(DateTimeHelper.now());
            base.setEstimatedLoc(req.getEstimatedLoc());
            return base;
        }
    }

    /**
     * Used to deduplicate error messages on outgoing status data. This is to address an issue where in the past new and previous errors
     * weren't correctly deduplicated, leading to large amounts of duplicate messaging cluttering returns and user interfaces.
     * 
     * Added to address commits made previously to the fix for #160.
     * 
     * @param outgoing commit data that should have the status errors filtered to remove duplicate messages
     * @return copy of the input list for easier chaining with updated statuses.
     */
    private List<CommitValidationStatus> deduplicateErrorMessages(List<CommitValidationStatus> outgoing) {
        if (outgoing == null) {
            return Collections.emptyList();
        }
        outgoing.forEach(s -> s.getErrors().removeIf(distinctByKey(err -> err.getCommit().getCommitHash() + err.getStatusCode())));
        return outgoing;
    }

    /**
     * Inversed uniqueness check that returns true if the record already exists in the predicate map.
     * 
     * @param <T> type of entry being filtered
     * @param keyExtractor function that retrieves unique keys for entries
     * @return predicate for deduplicating in removal situations
     */
    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        KeySetView<Object, Boolean> seen = ConcurrentHashMap.newKeySet();
        return t -> !seen.add(keyExtractor.apply(t));
    }
}
