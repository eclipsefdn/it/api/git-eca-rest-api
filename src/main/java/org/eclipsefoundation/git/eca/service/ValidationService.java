/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.service;

import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.model.ValidationResponse;
import org.eclipsefoundation.http.model.RequestWrapper;

/**
 * Service containing logic for validating commits.
 * 
 * @author Martin Lowe
 *
 */
public interface ValidationService {

    /**
     * Validate an incoming request, checking to make sure that ECA is present for users interacting with the API, as well
     * as maintain access rights to spec projects, blocking non-elevated access to the repositories.
     * 
     * @param req the request to validate
     * @param wrapper the current request wrapper
     * @return the validation results to return to the user.
     */
    public ValidationResponse validateIncomingRequest(ValidationRequest req, RequestWrapper wrapper);

}
