/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.service.impl;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.model.CacheWrapper;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;
import org.eclipsefoundation.efservices.services.ProfileService;
import org.eclipsefoundation.git.eca.api.BotsAPI;
import org.eclipsefoundation.git.eca.config.MailValidationConfig;
import org.eclipsefoundation.git.eca.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MultivaluedHashMap;

/**
 * Wrapped cached and authenticated access to user objects.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class CachedUserService implements UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CachedUserService.class);

    @Inject
    MailValidationConfig config;

    @Inject
    CachingService cache;
    @Inject
    ProfileService profile;

    // eclipse API rest client interfaces
    @RestClient
    BotsAPI bots;

    // rendered list of regex values
    List<Pattern> patterns;

    @PostConstruct
    void init() {
        // compile the patterns once per object to save processing time
        this.patterns = config.noreplyEmailPatterns().stream().map(Pattern::compile).toList();
    }

    @Override
    public EfUser getUser(String mail) {
        if (StringUtils.isBlank(mail)) {
            return null;
        }
        CacheWrapper<EfUser> result = cache.get(mail, new MultivaluedHashMap<>(), EfUser.class, () -> retrieveUser(mail));
        Optional<EfUser> user = result.data();
        if (user.isPresent()) {
            LOGGER.debug("Found user with email {}", mail);
            return user.get();
        }
        LOGGER.debug("Could not find user with email {}", mail);
        return null;
    }

    @Override
    public EfUser getUserByGithubUsername(String username) {
        if (StringUtils.isBlank(username)) {
            return null;
        }
        Optional<EfUser> user = profile.fetchUserByGhHandle(username, true);
        if (user.isPresent()) {
            LOGGER.debug("Found user with name {}", username);
            return user.get();
        }
        LOGGER.debug("Could not find user with name {}", username);
        return null;
    }

    @Override
    public boolean userIsABot(String mail, List<Project> filteredProjects) {
        if (StringUtils.isBlank(mail)) {
            return false;
        }
        // set the values used to validate whether user is a bot
        final String checkedField;
        final String checkedValue;
        // check if email is a noreply address
        if (patterns.stream().anyMatch(pattern -> pattern.matcher(mail.trim()).find())) {
            checkedValue = extractNoReplyUserFromAddress(mail);
            checkedField = "username";
            LOGGER.trace("Checking {} for bot status using noreply logic", checkedValue);
        } else {
            checkedValue = mail;
            checkedField = "email";
            LOGGER.trace("Checking {} for bot status using standard logic", checkedValue);
        }

        // get the bots to be compared against
        List<JsonNode> botObjs = getBots();
        // if there are no matching projects, then check against all bots, not just
        // project bots
        if (filteredProjects == null || filteredProjects.isEmpty()) {
            return botObjs.stream().anyMatch(bot -> checkFieldsForMatchingMail(bot, mail));
        }
        // check if any of the projects match any of the bots
        return filteredProjects.stream().anyMatch(p -> botObjs.stream().anyMatch(bot -> {
            LOGGER.debug("Checking project {} for matching bots", p.projectId());
            return p.projectId().equalsIgnoreCase(bot.get("projectId").asText())
                    && checkFieldsForMatchingValue(bot, checkedValue, checkedField);
        }));

    }

    /**
     * Checks for standard and noreply email address matches for a Git user and converts to a Eclipse Foundation account object.
     * 
     * @param user the user to attempt account retrieval for.
     * @return the user account if found by mail, or null if none found.
     */
    private EfUser retrieveUser(String mail) {
        if (StringUtils.isBlank(mail)) {
            LOGGER.debug("Blank mail passed, cannot fetch user");
            return null;
        }
        LOGGER.debug("Getting fresh user for {}", mail);
        // check for noreply (no reply will never have user account, and fails fast)
        EfUser noReplyUser = checkForNoReplyUser(mail);
        if (noReplyUser != null) {
            return noReplyUser;
        }
        // standard user check (returns best match)
        LOGGER.debug("Checking user with mail {}", mail);
        try {
            Optional<EfUser> user = profile.performUserSearch(new UserSearchParams(null, null, mail));
            if (user.isPresent()) {
                return user.get();
            }
        } catch (WebApplicationException e) {
            LOGGER.warn("Could not find user account with mail '{}'", mail);
        }
        return null;
    }

    /**
     * Checks git user for no-reply address, and attempts to ratify user through reverse lookup in API service. Currently, this service only
     * recognizes Github no-reply addresses as they have a route to be mapped.
     * 
     * @param user the Git user account to check for no-reply mail address
     * @return the Eclipse user if email address is detected no reply and one can be mapped, otherwise null
     */
    private EfUser checkForNoReplyUser(String mail) {
        if (StringUtils.isBlank(mail)) {
            LOGGER.debug("Blank mail passed, cannot fetch user");
            return null;
        }
        LOGGER.debug("Checking user with mail {} for no-reply", mail);
        boolean isNoReply = patterns.stream().anyMatch(pattern -> pattern.matcher(mail.trim()).find());
        if (isNoReply) {
            // get the user from the noreply address
            String uname = extractNoReplyUserFromAddress(mail);
            LOGGER.debug("User with mail {} detected as noreply account, checking services for username match on '{}'", mail, uname);

            // check github for no-reply (only allowed noreply currently)
            if (mail.endsWith("noreply.github.com")) {
                try {
                    // check for Github no reply, return if set
                    return getUserByGithubUsername(uname);
                } catch (WebApplicationException e) {
                    LOGGER.warn("No match for '{}' in Github", uname);
                }
            }
        }
        return null;
    }

    /**
     * Checks JSON node to look for email fields, both at the root, and nested email fields.
     * 
     * @param bot the bots JSON object representation
     * @param mail the email to match against
     * @return true if the bot has a matching email value, otherwise false
     */
    private boolean checkFieldsForMatchingMail(JsonNode bot, String mail) {
        return checkFieldsForMatchingValue(bot, mail, "email");
    }

    private boolean checkFieldsForMatchingValue(JsonNode bot, String targetValue, String fieldName) {
        // check the root email for the bot for match
        JsonNode botField = bot.get(fieldName);
        if (targetValue != null && botField != null && targetValue.equalsIgnoreCase(botField.asText(""))) {
            LOGGER.debug("Found matching bot at root level for '{}'", targetValue);
            return true;
        }
        Iterator<Entry<String, JsonNode>> i = bot.fields();
        while (i.hasNext()) {
            Entry<String, JsonNode> e = i.next();
            // check that our field is an object with fields
            JsonNode node = e.getValue();
            if (node.isObject()) {
                LOGGER.debug("Checking {} for bot value in field {}", e.getKey(), fieldName);
                // if the mail matches (ignoring case) user is bot
                JsonNode botAliasField = node.get(fieldName);
                if (targetValue != null && botAliasField != null && targetValue.equalsIgnoreCase(botAliasField.asText(""))) {
                    LOGGER.debug("Found match for bot value {}", targetValue);
                    return true;
                }
            }
        }
        return false;
    }

    private String extractNoReplyUserFromAddress(String mail) {
        // get the username/ID string before the first @ symbol.
        String noReplyUser = mail.substring(0, mail.indexOf("@", 0));
        // split based on +, if more than one part, use second (contains user),
        // otherwise, use whole string
        String[] nameParts = noReplyUser.split("\\+");
        String namePart;
        if (nameParts.length > 1 && nameParts[1] != null) {
            namePart = nameParts[1];
        } else {
            namePart = nameParts[0];
        }
        return namePart.trim();
    }

    private List<JsonNode> getBots() {
        return cache
                .get("allBots", new MultivaluedHashMap<>(), JsonNode.class, () -> bots.getBots())
                .data()
                .orElse(Collections.emptyList());
    }

}
