/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.service.impl;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUserBuilder;
import org.eclipsefoundation.efservices.api.models.EfUserCountryBuilder;
import org.eclipsefoundation.efservices.api.models.EfUserEcaBuilder;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.git.eca.config.ECAParallelProcessingConfig;
import org.eclipsefoundation.git.eca.config.MailValidationConfig;
import org.eclipsefoundation.git.eca.dto.CommitValidationStatus;
import org.eclipsefoundation.git.eca.helper.CommitHelper;
import org.eclipsefoundation.git.eca.helper.ProjectHelper;
import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.CommitStatus;
import org.eclipsefoundation.git.eca.model.GitUser;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.model.ValidationResponse;
import org.eclipsefoundation.git.eca.model.ValidationResponsePart;
import org.eclipsefoundation.git.eca.namespace.APIStatusCode;
import org.eclipsefoundation.git.eca.service.UserService;
import org.eclipsefoundation.git.eca.service.ValidationService;
import org.eclipsefoundation.git.eca.service.ValidationStatusService;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * Default service for validating external requests for ECA validation, as well as storing and retrieving information about historic
 * requests.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class DefaultValidationService implements ValidationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultValidationService.class);

    @Inject
    MailValidationConfig config;
    @Inject
    ECAParallelProcessingConfig parallelProcessingConfig;

    @Inject
    ProjectHelper projects;
    @Inject
    UserService users;
    @Inject
    ValidationStatusService statusService;

    @Inject
    ManagedExecutor executor;

    @Override
    public ValidationResponse validateIncomingRequest(ValidationRequest req, RequestWrapper wrapper) {
        // get the projects associated with the current request, if any
        List<Project> filteredProjects = projects.retrieveProjectsForRequest(req);

        // check to make sure commits are valid
        if (req.getCommits().stream().anyMatch(c -> !CommitHelper.validateCommit(c))) {
            // generate the error response object to load in the request errors
            ValidationResponse r = generateBaseResponse(req, filteredProjects);
            // for each invalid commit, add errors to output and return
            req
                    .getCommits()
                    .stream()
                    .filter(c -> !CommitHelper.validateCommit(c))
                    .forEach(c -> r
                            .addError(c.getHash(), "One or more commits were invalid. Please check the payload and try again",
                                    APIStatusCode.ERROR_DEFAULT));
            return r;
        }

        // get previous validation status messages
        List<CommitValidationStatus> statuses = statusService
                .getRequestCommitValidationStatus(wrapper, req, filteredProjects.isEmpty() ? null : filteredProjects.get(0).projectId());
        List<ValidationResponsePart> parts;
        if (parallelProcessingConfig.enabled()) {
            LOGGER.trace("Using parallel for commit processing");
            parts = doParallelProcessing(req, filteredProjects, statuses);
        } else {
            LOGGER.trace("Using serial for commit processing");
            parts = doSerialProcessing(req, filteredProjects, statuses);
        }
        ValidationResponse r = parts
                .stream()
                .filter(Objects::nonNull)
                .collect(() -> generateBaseResponse(req, filteredProjects),
                        (response, part) -> response.getCommits().put(part.hash(), part.commit()),
                        (response1, response2) -> response1.getCommits().putAll(response2.getCommits()));
        // update the persisted statuses to the db before returning
        statusService.updateCommitValidationStatus(wrapper, r, req, statuses, filteredProjects.isEmpty() ? null : filteredProjects.get(0));
        return r;
    }

    /**
     * Using serial/blocking processing, process the validation request.
     * 
     * @param req the request to validate
     * @param filteredProjects tracked projects for the current request
     * @param statuses list containing historic status history for the given request
     * @return validated response parts for the request to be recombined, persisted, and returned.
     */
    private List<ValidationResponsePart> doSerialProcessing(ValidationRequest req, List<Project> filteredProjects,
            List<CommitValidationStatus> statuses) {
        return req
                .getCommits()
                .stream()
                .map(c -> checkAndProcessCommit(c, filteredProjects, Boolean.TRUE.equals(req.getStrictMode()), statuses))
                .toList();
    }

    /**
     * Using parallel processing, process the validation request.
     * 
     * @param req the request to validate
     * @param filteredProjects tracked projects for the current request
     * @param statuses list containing historic status history for the given request
     * @return validated response parts for the request to be recombined, persisted, and returned.
     */
    private List<ValidationResponsePart> doParallelProcessing(ValidationRequest req, List<Project> filteredProjects,
            List<CommitValidationStatus> statuses) {
        // process the commits in parallel. While this may cause some delays with caching, should be overall faster
        Uni<List<ValidationResponsePart>> out = Uni
                .combine()
                .all()
                .unis(req
                        .getCommits()
                        .stream()
                        .map(i -> Uni
                                .createFrom()
                                .completionStage(() -> executor
                                        .supplyAsync(() -> checkAndProcessCommit(i, filteredProjects,
                                                Boolean.TRUE.equals(req.getStrictMode()), statuses))))
                        .toList())
                .usingConcurrencyOf(parallelProcessingConfig.threadsPerValidation())
                .with(ValidationResponsePart.class, list -> list);

        LOGGER.trace("Starting parallel processing w/ lvl {} concurrency", parallelProcessingConfig.threadsPerValidation());
        return out.await().atMost(Duration.of(30, ChronoUnit.SECONDS));
    }

    /**
     * Checks historic records, returning those if applicable. Otherwise, a fresh validation is calculated for the commit.
     * 
     * @param c the commit to process
     * @param filteredProjects tracked projects for the current request
     * @param useStrict whether or not strict validation should be used when processing errors.
     * @param statuses list containing historic status history for the given request
     * @return the validation status part for the current commit
     */
    private ValidationResponsePart checkAndProcessCommit(Commit c, List<Project> filteredProjects, boolean useStrict,
            List<CommitValidationStatus> statuses) {
        LOGGER.trace("Processing {}", c.getHash());
        // get the current status if present
        Optional<CommitValidationStatus> status = statuses.stream().filter(s -> s.getCommitHash().equals(c.getHash())).findFirst();
        // skip the commit validation if already passed
        if (isValidationStatusCurrentAndValid(status, c)) {
            CommitStatus commitStatus = CommitStatus.builder().build();
            commitStatus.addMessage("Commit was previously validated, skipping processing", APIStatusCode.SUCCESS_SKIPPED);
            return new ValidationResponsePart(commitStatus, c.getHash(), !filteredProjects.isEmpty(), useStrict);
        }
        // process the current commit
        return processUnvalidatedCommit(c, filteredProjects, useStrict);
    }

    /**
     * Process the current request, validating that the passed commit is valid. The author and committers Eclipse Account is retrieved,
     * which are then used to check if the current commit is valid for the current project.
     *
     * @param c the commit to process
     * @param filteredProjects tracked projects for the current request
     * @param useStrict whether or not strict validation should be used when processing errors.
     */
    private ValidationResponsePart processUnvalidatedCommit(Commit c, List<Project> filteredProjects, boolean useStrict) {
        // build the containers for the response messages
        CommitStatus status = CommitStatus.builder().build();
        ValidationResponsePart part = new ValidationResponsePart(status, c.getHash(), !filteredProjects.isEmpty(), useStrict);

        // retrieve the author + committer for the current request
        GitUser author = c.getAuthor();
        status.addMessage(String.format("Reviewing commit: %1$s", c.getHash()));
        status.addMessage(String.format("Authored by: %1$s <%2$s>", author.getName(), author.getMail()));

        // skip processing if a merge commit
        List<String> parents = c.getParents();
        if (parents != null && parents.size() > 1) {
            status.addMessage(String.format("Commit '%1$s' has multiple parents, merge commit detected, passing", c.getHash()));
            return part;
        }

        // retrieve the eclipse account for the author
        EfUser eclipseAuthor = getIdentifiedUser(author);
        // if the user is a bot, generate a stubbed user
        if (isAllowedUser(author.getMail()) || users.userIsABot(author.getMail(), filteredProjects)) {
            status.addMessage(String.format("Automated user '%1$s' detected for author of commit %2$s", author.getMail(), c.getHash()));
            eclipseAuthor = createBotStub(author);
        } else if (eclipseAuthor == null) {
            status
                    .addMessage(String
                            .format("Could not find an Eclipse user with mail '%1$s' for author of commit %2$s", author.getMail(),
                                    c.getHash()));
            part.addError("Author must have an Eclipse Account", APIStatusCode.ERROR_AUTHOR_NOT_FOUND);
            return part;
        }

        GitUser committer = c.getCommitter();
        // retrieve the eclipse account for the committer
        EfUser eclipseCommitter = getIdentifiedUser(committer);
        // check if whitelisted or bot
        if (isAllowedUser(committer.getMail()) || users.userIsABot(committer.getMail(), filteredProjects)) {
            status
                    .addMessage(
                            String.format("Automated user '%1$s' detected for committer of commit %2$s", committer.getMail(), c.getHash()));
            eclipseCommitter = createBotStub(committer);
        } else if (eclipseCommitter == null) {
            status
                    .addMessage(String
                            .format("Could not find an Eclipse user with mail '%1$s' for committer of commit %2$s", committer.getMail(),
                                    c.getHash()));
            part.addError("Committing user must have an Eclipse Account", APIStatusCode.ERROR_COMMITTER_NOT_FOUND);
            return part;
        }
        // validate author access to the current repo
        validateUserAccess(part, eclipseAuthor, filteredProjects, APIStatusCode.ERROR_AUTHOR);

        // check committer general access
        boolean isCommittingUserCommitter = isCommitter(part, eclipseCommitter, filteredProjects);
        validateUserAccessPartial(part, eclipseCommitter, isCommittingUserCommitter, APIStatusCode.ERROR_COMMITTER);
        return part;
    }

    /**
     * Validates author access for the current commit. If there are errors, they are recorded in the response for the current request to be
     * returned once all validation checks are completed.
     *
     * @param part the current commits validation information
     * @param eclipseUser the user to validate on a branch
     * @param filteredProjects tracked projects for the current request
     * @param errorCode the error code to display if the user does not have access
     */
    private void validateUserAccess(ValidationResponsePart part, EfUser eclipseUser, List<Project> filteredProjects,
            APIStatusCode errorCode) {
        // call isCommitter inline and pass to partial call
        validateUserAccessPartial(part, eclipseUser, isCommitter(part, eclipseUser, filteredProjects), errorCode);
    }

    /**
     * Allows for isCommitter to be called external to this method. This was extracted to ensure that isCommitter isn't called twice for the
     * same user when checking committer proxy push rules and committer general access.
     * 
     * @param part the current commits validation information
     * @param eclipseUser the user to validate on a branch
     * @param isCommitter the results of the isCommitter call from this class.
     * @param errorCode the error code to display if the user does not have access
     */
    private void validateUserAccessPartial(ValidationResponsePart part, EfUser eclipseUser, boolean isCommitter, APIStatusCode errorCode) {
        String userType = "author";
        if (APIStatusCode.ERROR_COMMITTER.equals(errorCode)) {
            userType = "committer";
        }
        if (isCommitter) {
            part
                    .commit()
                    .addMessage(String.format("Eclipse user '%s'(%s) is a committer on the project.", eclipseUser.name(), userType));
        } else {
            part
                    .commit()
                    .addMessage(String.format("Eclipse user '%s'(%s) is not a committer on the project.", eclipseUser.name(), userType));
            // check if the author is signed off if not a committer
            if (eclipseUser.eca().signed()) {
                part
                        .commit()
                        .addMessage(String
                                .format("Eclipse user '%s'(%s) has a current Eclipse Contributor Agreement (ECA) on file.",
                                        eclipseUser.name(), userType));
            } else {
                part
                        .commit()
                        .addMessage(String
                                .format("Eclipse user '%s'(%s) does not have a current Eclipse Contributor Agreement (ECA) on file.\n"
                                        + "If there are multiple commits, please ensure that each author has a ECA.", eclipseUser.name(),
                                        userType));
                part
                        .addError(String
                                .format("An Eclipse Contributor Agreement is required for Eclipse user '%s'(%s).", eclipseUser.name(),
                                        userType),
                                errorCode);
            }
        }
    }

    /**
     * Checks whether the given user is a committer on the project. If they are and the project is also a specification for a working group,
     * an additional access check is made against the user.
     *
     * <p>
     * Additionally, a check is made to see if the user is a registered bot user for the given project. If they match for the given project,
     * they are granted committer-like access to the repository.
     *
     * @param part the current commits validation information
     * @param user the user to validate on a branch
     * @param filteredProjects tracked projects for the current request
     * @return true if user is considered a committer, false otherwise.
     */
    private boolean isCommitter(ValidationResponsePart part, EfUser user, List<Project> filteredProjects) {
        // iterate over filtered projects
        for (Project p : filteredProjects) {
            LOGGER.debug("Checking project '{}' for user '{}'", p.name(), user.name());
            // check if any of the committers usernames match the current user
            if (p.committers().stream().anyMatch(u -> u.username().equals(user.name()))) {
                // check if the current project is a committer project, and if the user can
                // commit to specs
                if (p.getSpecWorkingGroup().isPresent() && !user.eca().canContributeSpecProject()) {
                    // set error + update response status
                    part
                            .addError(String
                                    .format("Project is a specification for the working group '%1$s', but user does not have permission to modify a specification project",
                                            p.getSpecWorkingGroup()),
                                    APIStatusCode.ERROR_SPEC_PROJECT);
                    return false;
                } else {
                    LOGGER.debug("User '{}' was found to be a committer on current project repo '{}'", user.mail(), p.name());
                    return true;
                }
            }
        }
        // check if user is a bot, either through early detection or through on-demand check
        if ((user.isBot() != null && Boolean.TRUE.equals(user.isBot())) || users.userIsABot(user.mail(), filteredProjects)) {
            LOGGER.debug("User '{} <{}>' was found to be a bot", user.name(), user.mail());
            return true;
        }
        return false;
    }

    /**
     * Checks against internal allow list for global bypass users, like webmaster and dependabot.
     * 
     * @param mail the mail address to check for allow list
     * @return true if user email is in allow list, false otherwise
     */
    private boolean isAllowedUser(String mail) {
        return StringUtils.isNotBlank(mail) && config.allowList().indexOf(mail) != -1;
    }

    /**
     * Retrieves an Eclipse Account user object given the Git users email address (at minimum). This is facilitated using the Eclipse
     * Foundation accounts API, along short lived in-memory caching for performance and some protection against duplicate requests.
     *
     * @param user the user to retrieve Eclipse Account information for
     * @return the Eclipse Account user information if found, or null if there was an error or no user exists.
     */
    private EfUser getIdentifiedUser(GitUser user) {
        // check if the external ID is set, and if so, attempt to look the user up.
        if (StringUtils.isNotBlank(user.getExternalId())) {
            // right now this is only supported for Github account lookups, so that will be used
            EfUser actualUser = users.getUserByGithubUsername(user.getExternalId());
            // if present, return the user. Otherwise, log and continue processing
            if (actualUser != null) {
                return actualUser;
            } else {
                LOGGER
                        .debug("An external ID of {} was passed, but no matching Eclipse users found. Falling back on default email lookup.",
                                user.getExternalId());
            }
        }

        // don't process an empty email as it will never have a match
        if (StringUtils.isBlank(user.getMail())) {
            LOGGER.debug("Cannot get identified user if user is empty, returning null");
            return null;
        }
        // get the Eclipse account for the user
        try {
            // use cache to avoid asking for the same user repeatedly on repeated requests
            EfUser foundUser = users.getUser(user.getMail());
            if (foundUser == null) {
                LOGGER.warn("No users found for mail '{}'", user.getMail());
            }
            return foundUser;
        } catch (WebApplicationException e) {
            Response r = e.getResponse();
            if (r != null && r.getStatus() == Status.NOT_FOUND.getStatusCode()) {
                LOGGER.error("No users found for mail '{}'", user.getMail());
            } else {
                LOGGER.error("Error while checking for user", e);
            }
        }
        return null;
    }

    /**
     * <p>
     * Checks the following to determine whether a commit has already been validated:
     * </p>
     * <ul>
     * <li>The validation status exists
     * <li>The validation status has a user mail associated with it
     * <li>Validation status has no present errors
     * <li>Modification date is either unset or matches when set
     * <li>User mail is set and matches (ignores case)
     * </ul>
     * 
     * If any of these checks fail, then the commit should be revalidated.
     * 
     * @param status the current commits validation status if it exists
     * @param c the commit that is being validated
     * @return true if the commit does not need to be (re)validated, false otherwise.
     */
    private boolean isValidationStatusCurrentAndValid(Optional<CommitValidationStatus> status, Commit c) {
        return status.isPresent() && status.get().getErrors().isEmpty() && c.getAuthor() != null
                && StringUtils.isNotBlank(status.get().getUserMail())
                && status.get().getUserMail().equalsIgnoreCase(c.getAuthor().getMail())
                && (c.getLastModificationDate() == null || status.get().getLastModified().equals(c.getLastModificationDate()));
    }

    /**
     * Generates the base validation response to be returned based on the request and the projects associated with the current request.
     * 
     * @param req incoming request from client for validation
     * @param filteredProjects list of projects associated with the current request
     * @return the empty validation response object to be filled and returned.
     */
    private ValidationResponse generateBaseResponse(ValidationRequest req, List<Project> filteredProjects) {
        return ValidationResponse
                .builder()
                .setStrictMode(Boolean.TRUE.equals(req.getStrictMode()))
                .setTrackedProject(!filteredProjects.isEmpty())
                .setFingerprint(CommitHelper.generateRequestHash(req))
                .build();
    }

    private EfUser createBotStub(GitUser user) {
        return EfUserBuilder
                .builder()
                .uid("0")
                .picture("")
                .firstName("")
                .lastName("")
                .fullName("")
                .publisherAgreements(Collections.emptyMap())
                .twitterHandle("")
                .jobTitle("")
                .website("")
                .country(EfUserCountryBuilder.builder().build())
                .interests(Collections.emptyList())
                .name(user.getName())
                .mail(user.getMail())
                .eca(EfUserEcaBuilder.builder().build())
                .isBot(true)
                .build();
    }
}
