package org.eclipsefoundation.git.eca.resource;

import java.net.URI;

import io.quarkus.security.Authenticated;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

/**
 * Create a basic endpoint for enabling logins with redirects.
 */
@Authenticated
@Path("login")
public class OIDCResource {

    @GET
    public Response login(@QueryParam("redirect") URI redirect) {
        return Response.status(302).location(redirect).build();
    }
}
