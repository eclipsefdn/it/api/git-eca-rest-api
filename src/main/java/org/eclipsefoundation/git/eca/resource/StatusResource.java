/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.resource;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.git.eca.config.EclipseQuteTemplateExtensions;
import org.eclipsefoundation.git.eca.dto.CommitValidationStatus;
import org.eclipsefoundation.git.eca.helper.GithubValidationHelper;
import org.eclipsefoundation.git.eca.helper.ProjectHelper;
import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.model.ValidationResponse;
import org.eclipsefoundation.git.eca.namespace.ProviderType;
import org.eclipsefoundation.git.eca.service.GithubApplicationService;
import org.eclipsefoundation.git.eca.service.ValidationService;
import org.eclipsefoundation.git.eca.service.ValidationStatusService;

import io.quarkus.qute.Location;
import io.quarkus.qute.Template;
import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

/**
 * REST resource containing endpoints related to checking the status of validation requests.
 * 
 * @author Martin Lowe
 *
 */
@Path("eca/status")
public class StatusResource extends CommonResource {

    // parameter names for the status error page
    private static final String COMMIT_STATUSES_PARAMETER = "statuses";
    private static final String INCLUDE_INSTALL_LINK_PARAMETER = "includeInstallLink";
    private static final String MESSAGE_PARAMETER = "message";
    private static final String PULL_REQUEST_NUMBER_PARAMETER = "pullRequestNumber";
    private static final String REPO_URL_PARAMETER = "repoUrl";

    @Inject
    GithubApplicationService ghAppService;
    @Inject
    ValidationService validation;
    @Inject
    ValidationStatusService validationStatus;

    @Inject
    GithubValidationHelper validationHelper;
    @Inject
    ProjectHelper projects;

    @Inject
    UriInfo info;

    // Qute templates, generates UI status page
    @Location("simple_fingerprint_ui")
    Template statusUiTemplate;
    @Location("error")
    Template errorTemplate;

    /**
     * Standard endpoint for retrieving raw validation information on a historic request, using the fingerprint for lookups.
     * 
     * @param fingerprint the associated fingerprint with the request that was validated.
     * @return list of commits that exist for the given fingerprint, or empty if there are none that match
     */
    @GET
    @Path("{fingerprint}")
    public Response getCommitValidation(@PathParam("fingerprint") String fingerprint) {
        List<CommitValidationStatus> statuses = validationStatus.getHistoricValidationStatus(wrapper, fingerprint);
        // for each of the statuses, obfuscate the emails before return
        statuses.forEach(s -> {
            s.setUserMail(EclipseQuteTemplateExtensions.obfuscateEmail(s.getUserMail()));
            s.getErrors().stream().forEach(e -> {
                e.setAuthorEmail(EclipseQuteTemplateExtensions.obfuscateEmail(e.getAuthorEmail()));
                e.setCommitterEmail(EclipseQuteTemplateExtensions.obfuscateEmail(e.getCommitterEmail()));
            });
        });

        return Response.ok(statuses).build();
    }

    /**
     * Retrieves commit status information based on the fingerprint and builds a UI around the results for easier consumption.
     * 
     * @param fingerprint the string associated with the request for looking up related commit statuses.
     * @return the HTML UI for the status of the fingerprint request
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("{fingerprint}/ui")
    public Response getCommitValidationUI(@PathParam("fingerprint") String fingerprint) {
        List<CommitValidationStatus> statuses = validationStatus.getHistoricValidationStatus(wrapper, fingerprint);
        if (statuses.isEmpty()) {
            throw new NotFoundException(String.format("Fingerprint '%s' not found", fingerprint));
        }
        List<Project> ps = projects.retrieveProjectsForRepoURL(statuses.get(0).getRepoUrl(), statuses.get(0).getProvider());
        return Response
                .ok()
                .entity(statusUiTemplate
                        .data(COMMIT_STATUSES_PARAMETER, statuses)
                        .data(PULL_REQUEST_NUMBER_PARAMETER, null)
                        .data("repoName", null)
                        .data("orgName", null)
                        .data("project", ps.isEmpty() ? null : ps.get(0))
                        .data(REPO_URL_PARAMETER, statuses.get(0).getRepoUrl())
                        .data("installationId", null)
                        .data("currentUser", getUserForLoggedInAccount())
                        .data("redirectUri", info.getPath())
                        .render())
                .build();
    }

    /**
     * Retrieves and checks the validity of the commit statuses for a Github pull request, and if out of date will revalidate the request
     * automatically on load.
     * 
     * @param org the organization in Github that contains the target repo
     * @param repoName the name of the repo in Github containing the pull request
     * @param prNo the number of the pull request being targeted for the validation lookup
     * @return the status UI for the Github status lookup, or an error if there was a problem
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("gh/{org}/{repoName}/{prNo}")
    public Response getCommitValidationForGithub(@PathParam("org") String org, @PathParam("repoName") String repoName,
            @PathParam("prNo") Integer prNo) {
        // generate the URL used to retrieve valid projects
        String repoUrl = GithubValidationHelper.getRepoUrl(org, repoName);

        try {
            // check that the passed repo has a valid installation
            String installationId = ghAppService.getInstallationForRepo(org, repoName);
            if (StringUtils.isBlank(installationId)) {
                return Response
                        .ok()
                        .entity(errorTemplate
                                .data(MESSAGE_PARAMETER, "No Github ECA app installation could be found for the current pull request.")
                                .data(REPO_URL_PARAMETER, repoUrl)
                                .data(PULL_REQUEST_NUMBER_PARAMETER, prNo)
                                .data(INCLUDE_INSTALL_LINK_PARAMETER, true)
                                .render())
                        .build();
            }

            // get the data about the current request, and check that we have some validation statuses
            ValidationRequest req = validationHelper.generateRequest(installationId, org, repoName, prNo, repoUrl);
            List<CommitValidationStatus> statuses = validationStatus
                    .getHistoricValidationStatusByShas(wrapper, req.getCommits().stream().map(Commit::getHash).toList());
            // check if we have any data, and if there is none, attempt to validate the request information
            if (statuses.isEmpty()) {
                // run the validation for the current request adhoc
                ValidationResponse r = validationHelper.validateIncomingRequest(wrapper, org, repoName, prNo, false);
                if (r == null) {
                    return Response
                            .ok()
                            .entity(errorTemplate
                                    .data(MESSAGE_PARAMETER,
                                            "The currently selected PR is in a non-opened state, and will not be validated.")
                                    .data(REPO_URL_PARAMETER, repoUrl)
                                    .data(PULL_REQUEST_NUMBER_PARAMETER, prNo)
                                    .data(INCLUDE_INSTALL_LINK_PARAMETER, false)
                                    .render())
                            .build();
                }

                // retrieve the status of the commits to display on the status page
                statuses = validationStatus.getHistoricValidationStatusByShas(wrapper, r.getCommits().keySet().stream().toList());
            }

            // get projects for use in queries + UI
            List<Project> ps = projects.retrieveProjectsForRepoURL(repoUrl, ProviderType.GITHUB);
            // render and return the status UI
            return Response
                    .ok()
                    .entity(statusUiTemplate
                            .data(COMMIT_STATUSES_PARAMETER, statuses)
                            .data(PULL_REQUEST_NUMBER_PARAMETER, prNo)
                            .data("repoName", repoName)
                            .data("orgName", org)
                            .data("project", ps.isEmpty() ? null : ps.get(0))
                            .data(REPO_URL_PARAMETER, repoUrl)
                            .data("installationId", installationId)
                            .data("currentUser", getUserForLoggedInAccount())
                            .data("redirectUri", info.getPath())
                            .render())
                    .build();
        } catch (BadRequestException e) {
            return Response
                    .ok()
                    .entity(errorTemplate
                            .data(MESSAGE_PARAMETER,
                                    "Request made to validate content that is not eligible for validation (either closed, or with no identifiable changes to validate).")
                            .data(REPO_URL_PARAMETER, repoUrl)
                            .data(PULL_REQUEST_NUMBER_PARAMETER, prNo)
                            .data(INCLUDE_INSTALL_LINK_PARAMETER, false)
                            .render())
                    .build();
        } catch (WebApplicationException e) {
            return Response
                    .ok()
                    .entity(errorTemplate
                            .data(MESSAGE_PARAMETER,
                                    "Could not find a pull request given the passed parameters, please check the URL and try again.")
                            .data(REPO_URL_PARAMETER, repoUrl)
                            .data(PULL_REQUEST_NUMBER_PARAMETER, prNo)
                            .data(INCLUDE_INSTALL_LINK_PARAMETER, false)
                            .render())
                    .build();
        }
    }

}
