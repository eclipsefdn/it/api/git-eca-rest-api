/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*	      Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.services.ProfileService;
import org.eclipsefoundation.git.eca.helper.ProjectHelper;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.model.ValidationResponse;
import org.eclipsefoundation.git.eca.namespace.APIStatusCode;
import org.eclipsefoundation.git.eca.service.ValidationService;
import org.eclipsefoundation.utils.exception.FinalForbiddenException;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * Eca validation endpoint for Git commits. Will use information from the bots, projects, and accounts API to validate commits passed to
 * this endpoint. Should be as system agnostic as possible to allow for any service to request validation with less reliance on services
 * external to the Eclipse foundation.
 *
 * @author Martin Lowe, Zachary Sabourin
 */
@Path("/eca")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class ValidationResource extends CommonResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationResource.class);

    // external API/service harnesses
    @Inject
    CachingService cache;
    @Inject
    ProjectHelper projects;
    @Inject
    ValidationService validation;

    @Inject
    ProfileService profileService;

    /**
     * Consuming a JSON request, this method will validate all passed commits, using the repo URL and the repository provider. These commits
     * will be validated to ensure that all users are covered either by an Eca, or are committers on the project. In the case of Eca-only
     * contributors, an additional sign off footer is required in the body of the commit.
     *
     * @param req the request containing basic data plus the commits to be validated
     * @return a web response indicating success or failure for each commit, along with standard messages that may be used to give users
     * context on failure.
     */
    @POST
    public Response validate(ValidationRequest req) {
        List<String> messages = checkRequest(req);
        // only process if we have no errors
        if (messages.isEmpty()) {
            LOGGER.debug("Processing: {}", req);
            return validation.validateIncomingRequest(req, wrapper).toResponse();
        } else {
            // create a stubbed response with the errors
            ValidationResponse out = ValidationResponse.builder().build();
            messages.forEach(m -> out.addError(m, null, APIStatusCode.ERROR_DEFAULT));
            return out.toResponse();
        }
    }

    /**
     * Do a lookup of an email or username to check if the user has an ECA. Does basic checks on query to discover if email or username,
     * 
     * @param email deprecated field, to be removed end of 2024. Contains the email to lookup
     * @param query query string for lookup, can contain either an email address or username
     * @return
     */
    @GET
    @Path("/lookup")
    public Response getUserStatus(@QueryParam("email") String email, @QueryParam("q") String query) {
        // really basic check. A username will never have an @, while an email will always have it.
        boolean queryLikeEmail = query != null && query.contains("@");
        // check that the user has committer level access
        EfUser loggedInUser = getUserForLoggedInAccount();
        if (StringUtils.isNotBlank(email)) {
            // do the checks to see if the user is missing or has not signed the ECA using the legacy field
            handleUserEmailLookup(loggedInUser, email);
        } else if (queryLikeEmail) {
            // do the checks to see if the user is missing or has not signed the ECA
            handleUserEmailLookup(loggedInUser, query);
        } else if (StringUtils.isNotBlank(query)) {
            // if username is set, look up the user and check if it has an Eca
            Optional<EfUser> user = profileService.fetchUserByUsername(query, false);
            if (user.isEmpty()) {
                throw new NotFoundException(String.format("No user found with username '%s'", TransformationHelper.formatLog(query)));
            }
            if (!user.get().eca().signed()) {
                return Response.status(Status.FORBIDDEN).build();
            }
        } else {
            throw new BadRequestException("A username or email must be set to look up a user account");
        }
        return Response.ok().build();
    }

    /**
     * Check permissions on the logged in user, and if permitted, check if the designated email is associated with an EF account with a
     * valid ECA. If there is a problem or there is no match, a corresponding error will be thrown.
     * 
     * @param loggedInUser the currently logged in user converted to an EF user object
     * @param email the email to search for
     */
    private void handleUserEmailLookup(EfUser loggedInUser, String email) {
        // check if there is a user logged in, as this always requires authentication
        if (ident.isAnonymous()) {
            throw new BadRequestException("User must be logged in and have committer level access to search by email");
        }

        // check that user has a project relation as a way of checking user trust
        boolean isKnownCommitterOrPL = loggedInUser != null && loggedInUser.isCommitter();
        if (!isKnownCommitterOrPL) {
            throw new FinalForbiddenException("User must be logged in and have committer level access to search by email");
        }
        // do the lookup of the passed email
        EfUser user = users.getUser(email);
        if (user == null) {
            throw new NotFoundException(String.format("No user found with mail '%s'", TransformationHelper.formatLog(email)));
        }
        // if the user doesn't have a signed Eca, return an empty 403
        if (!user.eca().signed()) {
            throw new FinalForbiddenException("");
        }
    }

    /**
     * Check if there are any issues with the validation request, returning error messages if there are issues with the request.
     * 
     * @param req the current validation request
     * @return a list of error messages to report, or an empty list if there are no errors with the request.
     */
    private List<String> checkRequest(ValidationRequest req) {
        // check that we have commits to validate
        List<String> messages = new ArrayList<>();
        if (req.getCommits() == null || req.getCommits().isEmpty()) {
            messages.add("A commit is required to validate");
        }
        // check that we have a repo set
        if (req.getRepoUrl() == null || StringUtils.isBlank(req.getRepoUrl().getPath())) {
            messages.add("A base repo URL needs to be set in order to validate");
        }
        // check that we have a type set
        if (req.getProvider() == null) {
            messages.add("A provider needs to be set to validate a request");
        }
        return messages;
    }

}
