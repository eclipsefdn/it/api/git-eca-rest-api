/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.helper;

import java.io.FileReader;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.util.stream.Stream;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.git.eca.api.GithubAPI;
import org.eclipsefoundation.git.eca.api.models.GithubAccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.cache.CacheResult;
import io.smallrye.jwt.auth.principal.JWTParser;
import io.smallrye.jwt.build.Jwt;

/**
 * Helper to load external resources as a JWT key.
 * 
 * @author Martin Lowe
 *
 */
@Singleton
public class JwtHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtHelper.class);

    // security provider to use when ingesting the private key
    private static final String PROVIDER_NAME = "BC";

    @ConfigProperty(name = "smallrye.jwt.sign.key.location")
    String location;
    @ConfigProperty(name = "eclipse.github.default-api-version", defaultValue = "2022-11-28")
    protected String apiVersion;

    @RestClient
    protected GithubAPI ghApi;

    @Inject
    JWTParser parser;

    /**
     * Retrieve the bearer token string for the current installation.
     * 
     * @param installationId the installation to generate a bearer string for
     * @return the bearer string for the current request
     */
    public String getGhBearerString(String installationId) {
        return "Bearer " + getGithubAccessToken(installationId).getToken();
    }

    /**
     * Retrieve a new cached Github access token, using a JWT generated from the GH provided PKCS#1 private key.
     * 
     * @param installationId the installation that the access token is being generated for
     * @return the access token to be used in the requests.
     */
    @CacheResult(cacheName = "accesstoken")
    public GithubAccessToken getGithubAccessToken(String installationId) {
        return ghApi.getNewAccessToken("Bearer " + generateJwt(), apiVersion, installationId);
    }

    /**
     * Generate the JWT to use for Github requests. This is stored in a special one-shot cache method secured to this class,
     * and not to be used for other requests that require JWTs.
     * 
     * @return signed JWT using the issuer and secret defined in the secret properties.
     */
    public String generateJwt() {
        return Jwt.subject("EclipseWebmaster").sign(JwtHelper.getExternalPrivateKey(location));
    }

    /**
     * Reads in external PEM keys in a way that supports both PKCS#1 and PKCS#8. This is needed as the GH-provided RSA key
     * is encoded using PKCS#1 and is not available for consumption in OOTB Java/smallrye, and smallrye's default PEM reader
     * only reads within the resources path, so external file support isn't really available.
     * 
     * Src: https://stackoverflow.com/questions/41934846/read-rsa-private-key-of-format-pkcs1-in-java
     * 
     * @param location the location of the file
     * @return the PrivateKey instance for the PEM file at the location, or null if it could not be read/parsed.
     */
    public static PrivateKey getExternalPrivateKey(String location) {
        // do manual check of the provider to ensure presence before continuing
        Provider p = checkProviderPresence(PROVIDER_NAME);
        if (p == null) {
            LOGGER.error("Could not find provider for '{}' in the JDK security providers list, cannot continue", PROVIDER_NAME);
            return null;
        }

        // create auto-closing reading resources for the external PEM file
        try (FileReader keyReader = new FileReader(Paths.get(location).toFile()); PEMParser pemParser = new PEMParser(keyReader)) {
            // use the BouncyCastle provider for PKCS#1 support (not available ootb)
            JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider(p.getName());
            // create the key and retrieve the PrivateKey portion
            return converter.getKeyPair((PEMKeyPair) pemParser.readObject()).getPrivate();
        } catch (Exception e) {
            LOGGER.error("Error while loading private pem", e);
        }
        return null;
    }

    /**
     * Retrieves the target security provider given the name to assure presence. There was previously errors where this
     * could be missing, so checking manually to ensure presence can help detect this and add logging.
     * 
     * @param name security provider name to be looked up
     * @return the provider if present, or null.
     */
    private static Provider checkProviderPresence(String name) {
        return Stream.of(Security.getProviders()).filter(p -> p.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

}
