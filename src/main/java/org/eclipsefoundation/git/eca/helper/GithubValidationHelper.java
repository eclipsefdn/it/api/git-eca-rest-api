/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.helper;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.git.eca.api.GithubAPI;
import org.eclipsefoundation.git.eca.api.models.GithubCommit;
import org.eclipsefoundation.git.eca.api.models.GithubCommit.ParentCommit;
import org.eclipsefoundation.git.eca.api.models.GithubCommitStatusRequest;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.PullRequest;
import org.eclipsefoundation.git.eca.config.WebhooksConfig;
import org.eclipsefoundation.git.eca.dto.CommitValidationStatus;
import org.eclipsefoundation.git.eca.dto.GithubWebhookTracking;
import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.GitUser;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.model.ValidationResponse;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.namespace.GithubCommitStatuses;
import org.eclipsefoundation.git.eca.namespace.ProviderType;
import org.eclipsefoundation.git.eca.service.GithubApplicationService;
import org.eclipsefoundation.git.eca.service.ValidationService;
import org.eclipsefoundation.git.eca.service.ValidationStatusService;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.utils.helper.DateTimeHelper;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.ServerErrorException;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

/**
 * This class is used to adapt Github requests to the standard validation workflow in a way that could be reused by both resource calls and
 * scheduled tasks for revalidation.
 */
@ApplicationScoped
public class GithubValidationHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(GithubValidationHelper.class);

    private static final String VALIDATION_LOGGING_MESSAGE = "Setting validation state for {}/#{} to {}";

    @ConfigProperty(name = "eclipse.github.default-api-version", defaultValue = "2022-11-28")
    String apiVersion;

    @Inject
    WebhooksConfig webhooksConfig;

    @Inject
    JwtHelper jwtHelper;
    @Inject
    APIMiddleware middleware;
    @Inject
    PersistenceDao dao;
    @Inject
    FilterService filters;

    @Inject
    ValidationService validation;
    @Inject
    ValidationStatusService validationStatus;
    @Inject
    GithubApplicationService ghAppService;

    @RestClient
    GithubAPI ghApi;

    /**
     * Using the wrapper and the passed unique information about a Github validation instance, lookup or create the tracking request and
     * validate the data.
     * 
     * @param wrapper the wrapper for the current request
     * @param org the name of the GH organization
     * @param repoName the slug of the repo that has the PR to be validated
     * @param prNo the PR number for the current request.
     * @param forceRevalidation true if revalidation should be forced when there is no changes, false otherwise.
     * @return the validated response if it is a valid request, or throws a web exception if there is a problem validating the request.
     */
    public ValidationResponse validateIncomingRequest(RequestWrapper wrapper, String org, String repoName, Integer prNo,
            boolean forceRevalidation) {
        // use logging helper to sanitize newlines as they aren't needed here
        String fullRepoName = getFullRepoName(org, repoName);
        // get the installation ID for the given repo if it exists, and if the PR noted exists
        String installationId = ghAppService.getInstallationForRepo(org, repoName);
        Optional<PullRequest> prResponse = ghAppService.getPullRequest(installationId, org, repoName, prNo);
        if (StringUtils.isBlank(installationId)) {
            throw new BadRequestException("Could not find an ECA app installation for repo name: " + fullRepoName);
        } else if (prResponse.isEmpty()) {
            throw new NotFoundException(String.format("Could not find PR '%d' in repo name '%s'", prNo, fullRepoName));
        }

        // prepare the request for consumption
        String repoUrl = getRepoUrl(org, repoName);
        ValidationRequest vr = generateRequest(installationId, org, repoName, prNo, repoUrl);

        // build the commit sha list based on the prepared request
        List<String> commitShas = vr.getCommits().stream().map(Commit::getHash).toList();
        // there should always be commits for a PR, but in case, lets check
        if (commitShas.isEmpty()) {
            throw new BadRequestException(String.format("Could not find any commits for %s#%d", fullRepoName, prNo));
        }
        LOGGER.debug("Found {} commits for '{}#{}'", commitShas.size(), fullRepoName, prNo);

        // retrieve the webhook tracking info, or generate an entry to track this PR if it's missing.
        GithubWebhookTracking updatedTracking = retrieveAndUpdateTrackingInformation(wrapper, installationId, org, repoName,
                prResponse.get());
        if (updatedTracking == null) {
            throw new ServerErrorException("Error while attempting to revalidate request, try again later.",
                    Response.Status.INTERNAL_SERVER_ERROR);
        }

        // get the commit status of commits to use for checking historic validation
        List<CommitValidationStatus> statuses = validationStatus.getHistoricValidationStatusByShas(wrapper, commitShas);
        if (!"open".equalsIgnoreCase(prResponse.get().getState()) && statuses.isEmpty()) {
            throw new BadRequestException("Cannot find validation history for current non-open PR, cannot provide validation status");
        }

        // we only want to update/revalidate for open PRs, so don't do this check if the PR is merged/closed
        if (forceRevalidation) {
            LOGGER.debug("Forced revalidation for {}#{} has been started", fullRepoName, prNo);
            return handleGithubWebhookValidation(GithubWebhookRequest.buildFromTracking(updatedTracking), vr, wrapper);
        } else if ("open".equalsIgnoreCase(prResponse.get().getState()) && commitShas.size() != statuses.size()) {
            LOGGER.debug("Validation for {}#{} does not seem to be current, revalidating commits", fullRepoName, prNo);
            // using the updated tracking, perform the validation
            return handleGithubWebhookValidation(GithubWebhookRequest.buildFromTracking(updatedTracking), vr, wrapper);
        }
        return null;
    }

    /**
     * Generate a ValidationRequest object based on data pulled from Github, grabbing commits from the noted pull request using the
     * installation ID for access/authorization.
     * 
     * @param installationId the ECA app installation ID for the organization
     * @param repositoryFullName the full name of the repository where the PR resides
     * @param pullRequestNumber the pull request number that is being validated
     * @param repositoryUrl the URL of the repository that contains the commits to validate
     * @return the populated validation request for the Github request information
     */
    @SuppressWarnings("null")
    public ValidationRequest generateRequest(String installationId, String orgName, String repositoryName, int pullRequestNumber,
            String repositoryUrl) {
        checkRequestParameters(installationId, orgName, repositoryName, pullRequestNumber);
        // get the commits that will be validated, don't cache as changes can come in too fast for it to be useful
        if (LOGGER.isTraceEnabled()) {
            LOGGER
                    .trace("Retrieving commits for PR {} in repo {}/{}", pullRequestNumber, TransformationHelper.formatLog(orgName),
                            TransformationHelper.formatLog(repositoryName));
        }
        List<GithubCommit> commits = middleware
                .getAll(i -> ghApi
                        .getCommits(jwtHelper.getGhBearerString(installationId), apiVersion, orgName, repositoryName, pullRequestNumber));
        if (LOGGER.isTraceEnabled()) {
            LOGGER
                    .trace("Found {} commits for PR {} in repo {}/{}", commits.size(), pullRequestNumber,
                            TransformationHelper.formatLog(orgName), TransformationHelper.formatLog(repositoryName));
        }

        // set up the validation request from current data
        return ValidationRequest
                .builder()
                .setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create(repositoryUrl))
                .setStrictMode(true)
                .setCommits(commits
                        .stream()
                        .map(c -> Commit
                                .builder()
                                .setHash(c.getSha())
                                .setAuthor(GitUser
                                        .builder()
                                        .setMail(getNullableString(() -> c.getCommit().getAuthor().getEmail()))
                                        .setName(getNullableString(() -> c.getCommit().getAuthor().getName()))
                                        .setExternalId(getNullableString(() -> c.getAuthor().getLogin()))
                                        .build())
                                .setCommitter(GitUser
                                        .builder()
                                        .setMail(getNullableString(() -> c.getCommit().getCommitter().getEmail()))
                                        .setName(getNullableString(() -> c.getCommit().getCommitter().getName()))
                                        .setExternalId(getNullableString(() -> c.getCommitter().getLogin()))
                                        .build())
                                .setParents(c.getParents().stream().map(ParentCommit::getSha).toList())
                                .build())
                        .toList())
                .build();
    }

    /**
     * Process the current 'merge_group' request and create a commit status for the HEAD SHA. As commits need to pass branch rules including
     * the existing ECA check, we don't need to check the commits here.
     * 
     * @param request information about the request from the GH webhook on what resource requested revalidation. Used to target the commit
     * status of the resources
     */
    public void sendMergeQueueStatus(GithubWebhookRequest request) {
        if (request.getMergeGroup() == null) {
            throw new BadRequestException("Merge group object required in webhook request to send status for merge queue");
        }
        // send the success status for the head SHA
        ghApi
                .updateStatus(jwtHelper.getGhBearerString(request.getInstallation().getId()), apiVersion,
                        request.getRepository().getOwner().getLogin(), request.getRepository().getName(),
                        request.getMergeGroup().getHeadSha(),
                        GithubCommitStatusRequest
                                .builder()
                                .setDescription("Commits in merge group should be previously validated, auto-passing HEAD commit")
                                .setState(GithubCommitStatuses.SUCCESS.toString())
                                .setContext(webhooksConfig.github().context())
                                .build());
    }

    /**
     * Process the current request and update the checks state to pending then success or failure. Contains verbose TRACE logging for more
     * info on the states of the validation for more information
     * 
     * @param request information about the request from the GH webhook on what resource requested revalidation. Used to target the commit
     * status of the resources
     * @param vr the pseudo request generated from the contextual webhook data. Used to make use of existing validation logic.
     * @return true if the validation passed, false otherwise.
     */
    public ValidationResponse handleGithubWebhookValidation(GithubWebhookRequest request, ValidationRequest vr, RequestWrapper wrapper) {
        // null check the pull request to make sure that someone didn't push a bad value
        PullRequest pr = request.getPullRequest();
        if (pr == null) {
            throw new IllegalStateException("Pull request should not be null when handling validation");
        }

        // update the status before processing
        LOGGER.trace(VALIDATION_LOGGING_MESSAGE, request.getRepository().getFullName(), pr.getNumber(), GithubCommitStatuses.PENDING);
        updateCommitStatus(request, GithubCommitStatuses.PENDING);

        // validate the response
        LOGGER.trace("Begining validation of request for {}/#{}", request.getRepository().getFullName(), pr.getNumber());
        ValidationResponse r = validation.validateIncomingRequest(vr, wrapper);
        if (r.getPassed()) {
            LOGGER.trace(VALIDATION_LOGGING_MESSAGE, request.getRepository().getFullName(), pr.getNumber(), GithubCommitStatuses.SUCCESS);
            updateCommitStatus(request, GithubCommitStatuses.SUCCESS);
        } else {
            LOGGER.trace(VALIDATION_LOGGING_MESSAGE, request.getRepository().getFullName(), pr.getNumber(), GithubCommitStatuses.FAILURE);
            updateCommitStatus(request, GithubCommitStatuses.FAILURE);
        }
        return r;
    }

    /**
     * Shortcut method that will retrieve existing GH tracking info, create new entries if missing, and will update the state of existing
     * requests as well.
     * 
     * @param installationId the installation ID for the ECA app in the given repository
     * @param repositoryFullName the full repository name for the target repo, e.g. eclipse/jetty
     * @param pr the pull request targeted by the validation request.
     * @return a new or updated tracking object, or null if there was an error in saving the information
     */
    public GithubWebhookTracking retrieveAndUpdateTrackingInformation(RequestWrapper wrapper, String installationId, String orgName,
            String repositoryName, PullRequest pr) {
        return updateGithubTrackingIfMissing(wrapper,
                getExistingRequestInformation(wrapper, installationId, orgName, repositoryName, pr.getNumber()), pr, installationId,
                orgName, repositoryName);
    }

    /**
     * Checks if the Github tracking is present for the current request, and if missing will generate a new record and save it.
     * 
     * @param tracking the optional tracking entry for the current request
     * @param request the pull request that is being validated
     * @param installationId the ECA app installation ID for the current request
     * @param fullRepoName the full repo name for the validation request
     */
    public GithubWebhookTracking updateGithubTrackingIfMissing(RequestWrapper wrapper, Optional<GithubWebhookTracking> tracking,
            PullRequest request, String installationId, String orgName, String repositoryName) {
        String fullRepoName = getFullRepoName(orgName, repositoryName);
        // if there is no tracking present, create the missing tracking and persist it
        GithubWebhookTracking updatedTracking;
        if (tracking.isEmpty()) {
            updatedTracking = new GithubWebhookTracking();
            updatedTracking.setInstallationId(installationId);
            updatedTracking.setLastUpdated(DateTimeHelper.now());
            updatedTracking.setPullRequestNumber(request.getNumber());
            updatedTracking.setRepositoryFullName(fullRepoName);
            updatedTracking.setNeedsRevalidation(false);
            updatedTracking.setManualRevalidationCount(0);
            if (!"open".equalsIgnoreCase(request.getState())) {
                LOGGER
                        .warn("The PR {} in {} is not in an open state, and will not be validated to follow our validation practice",
                                updatedTracking.getPullRequestNumber(), fullRepoName);
            }
        } else {
            // uses the DB version as a base if available
            updatedTracking = tracking.get();
        }
        // always update the head SHA and the last known state
        updatedTracking.setLastKnownState(request.getState());
        updatedTracking.setHeadSha(request.getHead().getSha());

        // save the data, and log on its success or failure
        List<GithubWebhookTracking> savedTracking = dao
                .add(new RDBMSQuery<>(wrapper, filters.get(GithubWebhookTracking.class)), Arrays.asList(updatedTracking));
        if (savedTracking.isEmpty()) {
            LOGGER.warn("Unable to create new GH tracking record for request to validate {}#{}", fullRepoName, request.getNumber());
            return null;
        }
        // return the updated tracking when successful
        LOGGER.debug("Created/updated GH tracking record for request to validate {}#{}", fullRepoName, request.getNumber());
        return savedTracking.get(0);
    }

    /**
     * Attempts to retrieve a webhook tracking record given the installation, repository, and pull request number.
     * 
     * @param installationId the installation ID for the ECA app in the given repository
     * @param repositoryFullName the full repository name for the target repo, e.g. eclipse/jetty
     * @param pullRequestNumber the pull request number that is being processed currently
     * @return the webhook tracking record if it can be found, or an empty optional.
     */
    public Optional<GithubWebhookTracking> getExistingRequestInformation(RequestWrapper wrapper, String installationId, String orgName,
            String repositoryName, int pullRequestNumber) {
        checkRequestParameters(installationId, orgName, repositoryName, pullRequestNumber);
        String repositoryFullName = getFullRepoName(orgName, repositoryName);
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(GitEcaParameterNames.INSTALLATION_ID_RAW, installationId);
        params.add(GitEcaParameterNames.REPOSITORY_FULL_NAME_RAW, repositoryFullName);
        params.add(GitEcaParameterNames.PULL_REQUEST_NUMBER_RAW, Integer.toString(pullRequestNumber));
        return dao.get(new RDBMSQuery<>(wrapper, filters.get(GithubWebhookTracking.class), params)).stream().findFirst();
    }

    /**
     * Simple helper method so we don't have to repeat static strings in multiple places.
     * 
     * @param fullRepoName the full repo name including org for the target PR.
     * @return the full repo URL on Github for the request
     */
    public static String getRepoUrl(String org, String repoName) {
        return "https://github.com/" + org + '/' + repoName;
    }

    /**
     * Sends off a POST to update the commit status given a context for the current PR.
     * 
     * @param request the current webhook update request
     * @param state the state to set the status to
     * @param fingerprint the internal unique string for the set of commits being processed
     */
    private void updateCommitStatus(GithubWebhookRequest request, GithubCommitStatuses state) {
        PullRequest pr = request.getPullRequest();
        if (pr == null) {
            // should not be reachable, but for safety
            throw new IllegalStateException("Pull request should not be null when handling validation");
        }

        LOGGER
                .trace("Generated access token for installation {}: {}", request.getInstallation().getId(),
                        jwtHelper.getGithubAccessToken(request.getInstallation().getId()).getToken());
        ghApi
                .updateStatus(jwtHelper.getGhBearerString(request.getInstallation().getId()), apiVersion,
                        request.getRepository().getOwner().getLogin(), request.getRepository().getName(), pr.getHead().getSha(),
                        GithubCommitStatusRequest
                                .builder()
                                .setDescription(state.getMessage())
                                .setState(state.toString())
                                .setTargetUrl(webhooksConfig.github().serverTarget() + "/git/eca/status/gh/"
                                        + request.getRepository().getFullName() + '/' + pr.getNumber())
                                .setContext(webhooksConfig.github().context())
                                .build());
    }

    /**
     * Wraps a nullable value fetch to handle errors and will return null if the value can't be retrieved.
     * 
     * @param supplier the method with potentially nullable values
     * @return the value if it can be found, or null
     */
    private String getNullableString(Supplier<String> supplier) {
        try {
            return supplier.get();
        } catch (NullPointerException e) {
            // suppress, as we don't care at this point if its null
        }
        return null;
    }

    /**
     * Validates required fields for processing requests.
     * 
     * @param installationId the installation ID for the ECA app in the given repository
     * @param repositoryFullName the full repository name for the target repo, e.g. eclipse/jetty
     * @param pullRequestNumber the pull request number that is being processed currently
     * @throws BadRequestException if at least one of the parameters is in an invalid state.
     */
    private void checkRequestParameters(String installationId, String org, String repoName, int pullRequestNumber) {
        List<String> missingFields = new ArrayList<>();
        if (StringUtils.isBlank(installationId)) {
            missingFields.add(GitEcaParameterNames.INSTALLATION_ID_RAW);
        }
        if (StringUtils.isBlank(org)) {
            missingFields.add(GitEcaParameterNames.ORG_NAME_RAW);
        }
        if (StringUtils.isBlank(repoName)) {
            missingFields.add(GitEcaParameterNames.REPOSITORY_NAME_RAW);
        }
        if (pullRequestNumber < 1) {
            missingFields.add(GitEcaParameterNames.PULL_REQUEST_NUMBER_RAW);
        }

        // throw exception if some fields are missing as we can't continue to process the request
        if (!missingFields.isEmpty()) {
            throw new BadRequestException("Missing fields in order to prepare request: " + StringUtils.join(missingFields, ' '));
        }
    }
    
    /**
     * Retrieves the full repo name for a given org and repo name, used for storage and legacy support.
     *
     * @param org organization name being targeted
     * @param repo name of repo being targeted
     * @return the full repo name, following format of org/repo
     */
    public static String getFullRepoName(String org, String repo) {
        return TransformationHelper.formatLog(org + '/' + repo);
    }

}
