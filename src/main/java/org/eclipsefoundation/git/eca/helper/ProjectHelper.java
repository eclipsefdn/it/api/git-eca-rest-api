/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.helper;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.efservices.api.models.InterestGroup;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.efservices.api.models.ProjectBuilder;
import org.eclipsefoundation.efservices.api.models.ProjectGithubProjectBuilder;
import org.eclipsefoundation.efservices.api.models.ProjectProjectParticipantBuilder;
import org.eclipsefoundation.efservices.services.ProjectService;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.namespace.ProviderType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;

/**
 * Helps manage projects by providing filters on top of the generic service as well as operations like adapting interest groups to projects
 * for easier processing.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public final class ProjectHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectHelper.class);

    @Inject
    CachingService cache;
    @Inject
    ProjectService projects;

    public List<Project> retrieveProjectsForRequest(ValidationRequest req) {
        String repoUrl = req.getRepoUrl().getPath();
        if (repoUrl == null) {
            LOGGER.warn("Can not match null repo URL to projects");
            return Collections.emptyList();
        }
        return retrieveProjectsForRepoURL(repoUrl, req.getProvider());
    }

    public List<Project> retrieveProjectsForRepoURL(String repoUrl, ProviderType provider) {
        if (repoUrl == null) {
            LOGGER.warn("Can not match null repo URL to projects");
            return Collections.emptyList();
        }
        // check for all projects that make use of the given repo
        List<Project> availableProjects = getProjects();
        if (availableProjects.isEmpty()) {
            LOGGER.warn("Could not find any projects to match against");
            return Collections.emptyList();
        }
        LOGGER.debug("Checking projects for repos that end with: {}", repoUrl);

        // check if the website repo matches first, returning that project if it's found
        Optional<Project> siteProjectMatch = availableProjects
                .stream()
                .filter(p -> p.websiteRepo().stream().anyMatch(r -> repoUrl.equals(r.url())))
                .findFirst();
        if (siteProjectMatch.isPresent()) {
            LOGGER
                    .trace("Found website repo match for project '{}', assuming project and returning",
                            siteProjectMatch.get().projectId());
            return Arrays.asList(siteProjectMatch.get());
        }

        String projectNamespace = URI.create(repoUrl).getPath().substring(1).toLowerCase();
        // filter the projects based on the repo URL. At least one repo in project must match the repo URL to be valid
        switch (provider) {
            case GITLAB:
                return availableProjects.stream().filter(p -> doesProjectMatchGitlabRepos(p, repoUrl, projectNamespace)).toList();
            case GITHUB:
                return availableProjects.stream().filter(p -> doesProjectMatchGithubRepos(p, repoUrl, projectNamespace)).toList();
            case GERRIT:
                return availableProjects
                        .stream()
                        .filter(p -> p.gerritRepos().stream().anyMatch(re -> re.url() != null && re.url().endsWith(repoUrl)))
                        .toList();
            default:
                return Collections.emptyList();
        }
    }

    /**
     * Retrieve cached and adapted projects list to avoid having to create all interest group adaptations on every request.
     * 
     * @return list of available projects or empty list if none found.
     */
    public List<Project> getProjects() {
        return cache.get("all-combined", new MultivaluedHashMap<>(), Project.class, () -> {
            List<Project> availableProjects = projects.getAllProjects();
            availableProjects.addAll(adaptInterestGroups(projects.getAllInterestGroups()));
            return availableProjects;
        }).data().orElseGet(Collections::emptyList);
    }

    private List<Project> adaptInterestGroups(List<InterestGroup> igs) {
        return igs.stream().map(this::convertToProject).toList();
    }

    /**
     * Convert an interest group into a project, as they are essentially the same structure behind the API.
     * 
     * @param ig the interest group to convert
     * @return the project built from the interest group
     */
    private Project convertToProject(InterestGroup ig) {
        return ProjectBuilder
                .builder()
                .projectId(ig.projectId())
                .gerritRepos(Collections.emptyList())
                .gitlabRepos(Collections.emptyList())
                .githubRepos(Collections.emptyList())
                .gitlab(ig.gitlab())
                .github(ProjectGithubProjectBuilder.builder().org("").ignoredRepos(Collections.emptyList()).build())
                .committers(ig
                        .participants()
                        .stream()
                        .map(p -> ProjectProjectParticipantBuilder
                                .builder()
                                .fullName(p.fullName())
                                .url(p.url())
                                .username(p.username())
                                .build())
                        .toList())
                .projectLeads(ig
                        .leads()
                        .stream()
                        .map(p -> ProjectProjectParticipantBuilder
                                .builder()
                                .fullName(p.fullName())
                                .url(p.url())
                                .username(p.username())
                                .build())
                        .toList())
                .contributors(Collections.emptyList())
                .shortProjectId(ig.shortProjectId())
                .summary("")
                .websiteUrl("")
                .websiteRepo(Collections.emptyList())
                .industryCollaborations(Collections.emptyList())
                .releases(Collections.emptyList())
                .topLevelProject("")
                .url("")
                .logo(ig.logo())
                .tags(Collections.emptyList())
                .name(ig.title())
                .specProjectWorkingGroup(Collections.emptyMap())
                .build();
    }

    /**
     * Check if the passed projects Gitlab repos field has a match for the given repo URL, or if the Gitlab project group is set and matches
     * the project namespace of the request.
     * 
     * @param p project being checked
     * @param repoUrl URL of the repo that was the origin of the request
     * @param projectNamespace the namespace of the project of the repo that created the request.
     * @return true if the project is a match via Gitlab repos or project namespace, false otherwise
     */
    private boolean doesProjectMatchGitlabRepos(Project p, String repoUrl, String projectNamespace) {
        return p.gitlabRepos().stream().anyMatch(re -> re.url() != null && re.url().endsWith(repoUrl))
                || (projectNamespace.startsWith(p.gitlab().projectGroup() + "/")
                        && p.gitlab().ignoredSubGroups().stream().noneMatch(sg -> projectNamespace.startsWith(sg + "/")));
    }

    /**
     * Check if the passed projects Github repos field has a match for the given repo URL, or if the Github project organization is set and
     * matches the project namespace of the request.
     * 
     * @param p project being checked
     * @param repoUrl URL of the repo that was the origin of the request
     * @param projectNamespace the namespace of the project of the repo that created the request.
     * @return true if the project is a match via Github repos or organization, false otherwise
     */
    private boolean doesProjectMatchGithubRepos(Project p, String repoUrl, String projectNamespace) {
        return p.githubRepos().stream().anyMatch(re -> re.url() != null && re.url().endsWith(repoUrl))
                || (StringUtils.isNotBlank(p.github().org()) && projectNamespace.startsWith(p.github().org())
                        && p.github().ignoredRepos().stream().noneMatch(repoUrl::endsWith));
    }
}
