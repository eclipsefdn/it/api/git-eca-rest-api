/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.dto;

import java.time.ZonedDateTime;
import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.eclipsefoundation.persistence.model.SortableField;

/**
 * Github Webhook request tracking info. Tracking is required to trigger revalidation through the Github API from this
 * service.
 * 
 * @author Martin Lowe
 *
 */
@Entity
@Table
public class GithubWebhookTracking extends BareNode {
    public static final DtoTable TABLE = new DtoTable(GithubWebhookTracking.class, "gwt");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String installationId;
    private String repositoryFullName;
    private String headSha;
    private Integer pullRequestNumber;
    private String lastKnownState;
    @SortableField
    private ZonedDateTime lastUpdated;
    private boolean needsRevalidation;
    private Integer manualRevalidationCount;

    @Override
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the installationId
     */
    public String getInstallationId() {
        return installationId;
    }

    /**
     * @param installationId the installationId to set
     */
    public void setInstallationId(String installationId) {
        this.installationId = installationId;
    }

    /**
     * @return the repositoryFullName
     */
    public String getRepositoryFullName() {
        return repositoryFullName;
    }

    /**
     * @param repositoryFullName the repositoryFullName to set
     */
    public void setRepositoryFullName(String repositoryFullName) {
        this.repositoryFullName = repositoryFullName;
    }

    /**
     * @return the headSha
     */
    public String getHeadSha() {
        return headSha;
    }

    /**
     * @param headSha the headSha to set
     */
    public void setHeadSha(String headSha) {
        this.headSha = headSha;
    }

    /**
     * @return the pullRequestNumber
     */
    public Integer getPullRequestNumber() {
        return pullRequestNumber;
    }

    /**
     * @param pullRequestNumber the pullRequestNumber to set
     */
    public void setPullRequestNumber(Integer pullRequestNumber) {
        this.pullRequestNumber = pullRequestNumber;
    }

    /**
     * @return the lastKnownState
     */
    public String getLastKnownState() {
        return lastKnownState;
    }

    /**
     * @param lastKnownState the lastKnownState to set
     */
    public void setLastKnownState(String lastKnownState) {
        this.lastKnownState = lastKnownState;
    }

    /**
     * @return the lastUpdated
     */
    public ZonedDateTime getLastUpdated() {
        return lastUpdated;
    }

    /**
     * @param lastUpdated the lastUpdated to set
     */
    public void setLastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    /**
     * @return the needsRevalidation
     */
    public boolean isNeedsRevalidation() {
        return needsRevalidation;
    }

    /**
     * @param needsRevalidation the needsRevalidation to set
     */
    public void setNeedsRevalidation(boolean needsRevalidation) {
        this.needsRevalidation = needsRevalidation;
    }

    /**
     * @return the manualRevalidationCount
     */
    public Integer getManualRevalidationCount() {
        return manualRevalidationCount;
    }

    /**
     * @param manualRevalidationCount the manualRevalidationCount to set
     */
    public void setManualRevalidationCount(Integer manualRevalidationCount) {
        this.manualRevalidationCount = manualRevalidationCount;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects
                .hash(headSha, id, installationId, lastKnownState, lastUpdated, manualRevalidationCount, needsRevalidation,
                        pullRequestNumber, repositoryFullName);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GithubWebhookTracking other = (GithubWebhookTracking) obj;
        return Objects.equals(headSha, other.headSha) && Objects.equals(id, other.id)
                && Objects.equals(installationId, other.installationId) && Objects.equals(lastKnownState, other.lastKnownState)
                && Objects.equals(lastUpdated, other.lastUpdated) && Objects.equals(manualRevalidationCount, other.manualRevalidationCount)
                && needsRevalidation == other.needsRevalidation && Objects.equals(pullRequestNumber, other.pullRequestNumber)
                && Objects.equals(repositoryFullName, other.repositoryFullName);
    }

    @Singleton
    public static class GithubWebhookTrackingFilter implements DtoFilter<GithubWebhookTracking> {

        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);

            String installationId = params.getFirst(GitEcaParameterNames.INSTALLATION_ID_RAW);
            if (StringUtils.isNotBlank(installationId)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".installationId = ?",
                                new Object[] { installationId }));
            }
            String pullRequestNumber = params.getFirst(GitEcaParameterNames.PULL_REQUEST_NUMBER_RAW);
            if (StringUtils.isNumeric(pullRequestNumber)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".pullRequestNumber = ?",
                                new Object[] { Integer.parseInt(pullRequestNumber) }));
            }
            String repositoryFullName = params.getFirst(GitEcaParameterNames.REPOSITORY_FULL_NAME_RAW);
            if (StringUtils.isNotBlank(repositoryFullName)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".repositoryFullName = ?",
                                new Object[] { repositoryFullName }));
            }
            String needsRevalidation = params.getFirst(GitEcaParameterNames.NEEDS_REVALIDATION_RAW);
            if (Boolean.TRUE.equals(Boolean.valueOf(needsRevalidation))) {
                statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".needsRevalidation = TRUE", new Object[] {}));
            }

            return statement;
        }

        @Override
        public Class<GithubWebhookTracking> getType() {
            return GithubWebhookTracking.class;
        }
    }
}
