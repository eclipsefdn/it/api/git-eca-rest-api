/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.dto;

import java.io.Serializable;
import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

@Entity
@Table
public class CommitValidationStatusGrouping extends BareNode {
    public static final DtoTable TABLE = new DtoTable(CommitValidationStatusGrouping.class, "cvsg");

    @EmbeddedId
    private GroupingCompositeId compositeId;

    public CommitValidationStatusGrouping() {
    }

    public CommitValidationStatusGrouping(String fingerprint, CommitValidationStatus commit) {
        this.compositeId = new GroupingCompositeId();
        this.compositeId.setFingerprint(fingerprint);
        this.compositeId.setCommit(commit);
    }

    @Override
    public GroupingCompositeId getId() {
        return getCompositeId();
    }

    /**
     * @return the compositeId
     */
    public GroupingCompositeId getCompositeId() {
        return compositeId;
    }

    /**
     * @param compositeId the compositeId to set
     */
    public void setCompositeId(GroupingCompositeId compositeId) {
        this.compositeId = compositeId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(compositeId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CommitValidationStatusGrouping other = (CommitValidationStatusGrouping) obj;
        return Objects.equals(compositeId, other.compositeId);
    }

    @Embeddable
    public static class GroupingCompositeId implements Serializable {
        private static final long serialVersionUID = 1L;

        private String fingerprint;
        @OneToOne
        private CommitValidationStatus commit;

        /**
         * @return the fingerprint
         */
        public String getFingerprint() {
            return fingerprint;
        }

        /**
         * @param fingerprint the fingerprint to set
         */
        public void setFingerprint(String fingerprint) {
            this.fingerprint = fingerprint;
        }

        /**
         * @return the commit
         */
        public CommitValidationStatus getCommit() {
            return commit;
        }

        /**
         * @param commit the commit to set
         */
        public void setCommit(CommitValidationStatus commit) {
            this.commit = commit;
        }

    }

    @Singleton
    public static class CommitValidationStatusGroupingFilter implements DtoFilter<CommitValidationStatusGrouping> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // fingerprint check
                String fingerprint = params.getFirst(GitEcaParameterNames.FINGERPRINT.getName());
                if (StringUtils.isNotBlank(fingerprint)) {
                    stmt
                            .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeId.fingerprint = ?",
                                    new Object[] { fingerprint }));
                }
                // commit id check
                String commitId = params.getFirst(GitEcaParameterNames.COMMIT_ID.getName());
                if (StringUtils.isNumeric(commitId)) {
                    stmt
                            .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeId.commit.id = ?",
                                    new Object[] { Integer.valueOf(commitId) }));
                }
            }
            return stmt;
        }

        @Override
        public Class<CommitValidationStatusGrouping> getType() {
            return CommitValidationStatusGrouping.class;
        }
    }
}
