/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

@Entity
@Table
public class PrivateProjectEvent extends BareNode {
    public static final DtoTable TABLE = new DtoTable(PrivateProjectEvent.class, "ppe");

    @EmbeddedId
    private EventCompositeId compositeId;
    @Column(name = "ef_username")
    private String efUsername;
    private Integer parentProject;
    private LocalDateTime creationDate;
    private LocalDateTime deletionDate;

    public PrivateProjectEvent() {

    }

    public PrivateProjectEvent(Integer userId, Integer projectId, String projectPath) {
        this.compositeId = new EventCompositeId();
        this.compositeId.setUserId(userId);
        this.compositeId.setProjectId(projectId);
        this.compositeId.setProjectPath(projectPath);
    }

    @Override
    public EventCompositeId getId() {
        return getCompositeId();
    }

    public EventCompositeId getCompositeId() {
        return this.compositeId;
    }

    public void setCompositeId(EventCompositeId compositeId) {
        this.compositeId = compositeId;
    }

    public String getEFUsername() {
        return this.efUsername;
    }

    public void setEFUsername(String efUsername) {
        this.efUsername = efUsername;
    }

    public Integer getParentProject() {
        return this.parentProject;
    }

    public void setParentProject(Integer parentProject) {
        this.parentProject = parentProject;
    }

    public LocalDateTime getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getDeletionDate() {
        return this.deletionDate;
    }

    public void setDeletionDate(LocalDateTime deletionDate) {
        this.deletionDate = deletionDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(compositeId, creationDate, deletionDate, efUsername, parentProject);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PrivateProjectEvent other = (PrivateProjectEvent) obj;
        return Objects.equals(compositeId, other.compositeId) && Objects.equals(creationDate, other.creationDate)
                && Objects.equals(deletionDate, other.deletionDate) && Objects.equals(efUsername, other.efUsername)
                && Objects.equals(parentProject, other.parentProject);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("PrivateProjectEvent [userId=");
        builder.append(getCompositeId().getUserId());
        builder.append(", projectId=");
        builder.append(getCompositeId().getProjectId());
        builder.append(", projectPath=");
        builder.append(getCompositeId().getProjectPath());
        builder.append(", ef_username=");
        builder.append(getEFUsername());
        builder.append(", parentProject=");
        builder.append(getParentProject());
        builder.append(", creationDate=");
        builder.append(getCreationDate());
        builder.append(", deletionDate=");
        builder.append(getDeletionDate());
        builder.append("]");
        return builder.toString();
    }

    @Embeddable
    public static class EventCompositeId implements Serializable {
        private static final long serialVersionUID = 1L;

        private Integer userId;
        private Integer projectId;
        private String projectPath;

        public Integer getUserId() {
            return this.userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getProjectId() {
            return this.projectId;
        }

        public void setProjectId(Integer projectId) {
            this.projectId = projectId;
        }

        public String getProjectPath() {
            return this.projectPath;
        }

        public void setProjectPath(String projectPath) {
            this.projectPath = projectPath;
        }
    }

    @Singleton
    public static class PrivateProjectEventFilter implements DtoFilter<PrivateProjectEvent> {

        private static final int LAST_HOUR_IN_DAY = (int) TimeUnit.HOURS.convert(1, TimeUnit.DAYS) - 1;
        private static final int LAST_MINUTE_IN_HOUR = (int) TimeUnit.MINUTES.convert(1, TimeUnit.HOURS) - 1;

        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);

            String userId = params.getFirst(GitEcaParameterNames.USER_ID.getName());
            if (StringUtils.isNumeric(userId)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeId.userId = ?",
                                new Object[] { Integer.valueOf(userId) }));
            }

            String projectId = params.getFirst(GitEcaParameterNames.PROJECT_ID.getName());
            if (StringUtils.isNumeric(projectId)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeId.projectId = ?",
                                new Object[] { Integer.valueOf(projectId) }));
            }

            List<String> projectIds = params.get(GitEcaParameterNames.PROJECT_IDS.getName());
            if (projectIds != null) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeId.projectId IN ?",
                                new Object[] { projectIds.stream().map(Integer::valueOf).toList() }));
            }

            List<String> notInProjectIds = params.get(GitEcaParameterNames.NOT_IN_PROJECT_IDS.getName());
            if (notInProjectIds != null) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeId.projectId NOT IN ?",
                                new Object[] { notInProjectIds.stream().map(Integer::valueOf).toList() }));

                statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".deletionDate IS NULL", new Object[] {}));
            }

            String projectPath = params.getFirst(GitEcaParameterNames.PROJECT_PATH.getName());
            if (StringUtils.isNotBlank(projectPath)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeId.projectPath = ?",
                                new Object[] { projectPath }));
            }

            String parentProject = params.getFirst(GitEcaParameterNames.PARENT_PROJECT.getName());
            if (StringUtils.isNumeric(parentProject)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".parentProject = ?",
                                new Object[] { Integer.valueOf(parentProject) }));
            }

            String active = params.getFirst(GitEcaParameterNames.STATUS_ACTIVE.getName());
            if (StringUtils.isNotBlank(active)) {
                statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".deletionDate IS NULL", new Object[] {}));
            }

            String deleted = params.getFirst(GitEcaParameterNames.STATUS_DELETED.getName());
            if (StringUtils.isNotBlank(deleted)) {
                statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".deletionDate IS NOT NULL", new Object[] {}));
            }

            String since = params.getFirst(GitEcaParameterNames.SINCE.getName());
            String until = params.getFirst(GitEcaParameterNames.UNTIL.getName());

            // Get date range or only one time filter
            if (StringUtils.isNotBlank(since) && StringUtils.isNotBlank(until)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".creationDate BETWEEN ? AND ?",
                                new Object[] { LocalDateTime.of(LocalDate.parse(since), LocalTime.of(0, 0)),
                                        LocalDateTime.of(LocalDate.parse(until), LocalTime.of(LAST_HOUR_IN_DAY, LAST_MINUTE_IN_HOUR)) }));
            } else if (StringUtils.isNotBlank(since)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".creationDate > ?",
                                new Object[] { LocalDateTime.of(LocalDate.parse(since), LocalTime.of(0, 0)) }));
            } else if (StringUtils.isNotBlank(until)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".creationDate < ?", new Object[] {
                                LocalDateTime.of(LocalDate.parse(until), LocalTime.of(LAST_HOUR_IN_DAY, LAST_MINUTE_IN_HOUR)) }));
            }

            return statement;
        }

        @Override
        public Class<PrivateProjectEvent> getType() {
            return PrivateProjectEvent.class;
        }
    }
}
