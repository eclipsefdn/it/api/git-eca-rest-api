/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.config;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * Captures all configs related to hcaptcha. Includes enabled flag, site key, and secret.
 */
@ConfigMapping(prefix = "eclipse.hcaptcha")
public interface HcaptchaConfig {

    @WithDefault("true")
    boolean enabled();

    String siteKey();

    String secret();
}
