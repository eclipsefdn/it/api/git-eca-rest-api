/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.git.eca.config;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * 
 */
@ConfigMapping(prefix = "eclipse.git-eca.parallel")
public interface ECAParallelProcessingConfig {

    @WithDefault("false")
    boolean enabled();

    @WithDefault("3")
    int threadsPerValidation();
}
