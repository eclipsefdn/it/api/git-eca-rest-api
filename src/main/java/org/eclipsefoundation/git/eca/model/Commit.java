/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.model;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import jakarta.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Represents a Git commit with basic data and metadata about the revision.
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_Commit.Builder.class)
public abstract class Commit {
    public abstract String getHash();

    @Nullable
    public abstract String getSubject();

    @Nullable
    public abstract String getBody();

    @Nullable
    public abstract List<String> getParents();

    public abstract GitUser getAuthor();

    public abstract GitUser getCommitter();

    @Nullable
    public abstract Boolean getHead();

    @Nullable
    public abstract ZonedDateTime getLastModificationDate();

    public static Builder builder() {
        return new AutoValue_Commit.Builder().setParents(new ArrayList<>());
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setHash(String hash);

        public abstract Builder setSubject(@Nullable String subject);

        public abstract Builder setBody(@Nullable String body);

        public abstract Builder setParents(@Nullable List<String> parents);

        public abstract Builder setAuthor(GitUser author);

        public abstract Builder setCommitter(GitUser committer);

        public abstract Builder setHead(@Nullable Boolean head);

        public abstract Builder setLastModificationDate(@Nullable ZonedDateTime lastModificationDate);

        public abstract Commit build();
    }
}
