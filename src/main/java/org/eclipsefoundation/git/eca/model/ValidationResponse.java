/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.model;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

import jakarta.annotation.Nullable;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import org.eclipsefoundation.git.eca.namespace.APIStatusCode;

import com.fasterxml.jackson.databind.PropertyNamingStrategies.LowerCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;
import com.google.auto.value.extension.memoized.Memoized;

/**
 * Represents an internal response for a call to this API.
 *
 * @author Martin Lowe
 */
@AutoValue
@JsonNaming(LowerCamelCaseStrategy.class)
@JsonDeserialize(builder = $AutoValue_ValidationResponse.Builder.class)
public abstract class ValidationResponse {
    public static final String NIL_HASH_PLACEHOLDER = "_nil";

    public abstract ZonedDateTime getTime();

    public abstract Map<String, CommitStatus> getCommits();

    public abstract boolean getTrackedProject();

    public abstract boolean getStrictMode();

    @Nullable
    public abstract String getFingerprint();

    public boolean getPassed() {
        return getErrorCount() <= 0;
    }

    @Memoized
    public int getErrorCount() {
        return getCommits().values().stream().mapToInt(s -> s.getErrors().size()).sum();
    }

    /** @param error message to add to the API response */
    public void addError(String hash, String error, APIStatusCode code) {
        if (this.getTrackedProject() || this.getStrictMode()) {
            getCommits().computeIfAbsent(getHashKey(hash), k -> CommitStatus.builder().build()).addError(error, code);
        } else {
            getCommits().computeIfAbsent(getHashKey(hash), k -> CommitStatus.builder().build()).addWarning(error, code);
        }
    }

    public static String getHashKey(String hash) {
        return hash == null ? NIL_HASH_PLACEHOLDER : hash;
    }

    /**
     * Converts the APIResponse to a web response with appropriate status.
     *
     * @return a web response with status {@link Status.OK} if the commits pass validation, {@link Status.FORBIDDEN} otherwise.
     */
    public Response toResponse() {
        // update error count before returning
        if (getPassed()) {
            return Response.ok(this).build();
        } else {
            return Response.status(Status.FORBIDDEN).entity(this).build();
        }
    }

    public static Builder builder() {
        return new AutoValue_ValidationResponse.Builder()
                .setStrictMode(false)
                .setTrackedProject(false)
                .setTime(ZonedDateTime.now())
                .setCommits(new HashMap<>());
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setTime(ZonedDateTime time);

        public abstract Builder setCommits(Map<String, CommitStatus> commits);

        public abstract Builder setTrackedProject(boolean trackedProject);

        public abstract Builder setStrictMode(boolean strictMode);

        public abstract Builder setFingerprint(@Nullable String fingerprint);

        public abstract ValidationResponse build();
    }
}
