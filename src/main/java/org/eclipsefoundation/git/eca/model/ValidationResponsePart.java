/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.model;

import org.eclipsefoundation.git.eca.namespace.APIStatusCode;

/**
 * Represents a part of the validation response for a single commit. Compartmentalized for better parallel processing.
 *
 * @author Martin Lowe
 */
public record ValidationResponsePart(CommitStatus commit, String hash, boolean isTrackedProject, boolean isStrict) {
    /**
     * Allows for the checks for tracked project + repsonse strictness to apply to messages more easily without changing the value returned
     * to the user.
     * 
     * @param error the error message to include with the current commit
     * @param code the status code associated with the message.
     */
    public void addError(String error, APIStatusCode code) {
        if (this.isTrackedProject || this.isStrict) {
            commit.addError(error, code);
        } else {
            commit.addWarning(error, code);
        }
    }
}
