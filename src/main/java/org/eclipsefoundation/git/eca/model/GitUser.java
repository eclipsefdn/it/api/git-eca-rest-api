/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.model;

import jakarta.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Basic object representing a Git users data required for verification.
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_GitUser.Builder.class)
public abstract class GitUser {
    @Nullable
    public abstract String getName();

    @Nullable
    public abstract String getMail();

    @Nullable
    public abstract String getExternalId();

    public static Builder builder() {
        return new AutoValue_GitUser.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setName(@Nullable String name);

        public abstract Builder setMail(@Nullable String mail);

        public abstract Builder setExternalId(@Nullable String externalId);

        public abstract GitUser build();
    }
}