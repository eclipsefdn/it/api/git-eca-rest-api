/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.tasks;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.git.eca.api.GitlabAPI;
import org.eclipsefoundation.git.eca.api.models.GitlabProjectResponse;
import org.eclipsefoundation.git.eca.api.models.GitlabUserResponse;
import org.eclipsefoundation.git.eca.dto.PrivateProjectEvent;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.arc.Arc;
import io.quarkus.arc.InstanceHandle;
import io.quarkus.scheduler.Scheduled;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

@ApplicationScoped
public class ScheduledPrivateProjectScanTask {
    public static final Logger LOGGER = LoggerFactory.getLogger(ScheduledPrivateProjectScanTask.class);

    private static final int PRIVATE_PROJECT_PAGE_SIZE = 100;

    @ConfigProperty(name = "eclipse.scheduled.private-project.enabled", defaultValue = "true")
    Instance<Boolean> enabled;
    @ConfigProperty(name = "eclipse.gitlab.access-token")
    String apiToken;

    @RestClient
    GitlabAPI api;

    @Inject
    CachingService cache;

    @Scheduled(every = "P7D")
    void reconcilePrivateProjects() {
        if (Boolean.FALSE.equals(enabled.get())) {
            LOGGER.warn("Private project scan task did not run. It has been disabled through configuration.");
        } else {
            InstanceHandle<APIMiddleware> middlewareHandle = Arc.container().instance(APIMiddleware.class);
            APIMiddleware middleware = middlewareHandle.get();

            // Fetch all private projects from GL, while paginating results
            Optional<List<GitlabProjectResponse>> response = cache
                    .get("all", new MultivaluedHashMap<>(), GitlabProjectResponse.class,
                            () -> middleware.getAll(p -> api.getPrivateProjects(p, apiToken, "private", PRIVATE_PROJECT_PAGE_SIZE)))
                    .data();

            response.ifPresent(this::processGitlabResponseList);
        }
    }

    /**
     * Processes the list of gitlab project response objects and updates the missed deletion, creation, and rename events in the DB
     * 
     * @param projectList the list of private projects to process
     */
    private void processGitlabResponseList(List<GitlabProjectResponse> projectList) {

        InstanceHandle<DefaultHibernateDao> daoHandle = Arc.container().instance(DefaultHibernateDao.class);
        DefaultHibernateDao dao = daoHandle.get();

        InstanceHandle<FilterService> filtersHandle = Arc.container().instance(FilterService.class);
        FilterService filters = filtersHandle.get();

        updateMissedDeletionEvents(projectList, dao, filters);
        updateMissedCreationAndRenameEvents(projectList, dao, filters);
    }

    /**
     * Updates the DB records that aren't included in the Gitlab project list.
     * 
     * @param projectList The list of private projects to process
     * @param dao The dao service
     * @param filters The filters service
     */
    private void updateMissedDeletionEvents(List<GitlabProjectResponse> projectList, DefaultHibernateDao dao, FilterService filters) {
        RequestWrapper wrapper = new FlatRequestWrapper(URI.create("https://api.eclipse.org"));

        // Ad ids to be reverse searched against
        MultivaluedMap<String, String> excludingParams = new MultivaluedHashMap<>();
        excludingParams
                .put(GitEcaParameterNames.NOT_IN_PROJECT_IDS.getName(),
                        projectList.stream().map(p -> Integer.toString(p.getId())).toList());

        // Get all excluding the ids found on GL
        List<PrivateProjectEvent> deletedResults = dao
                .get(new RDBMSQuery<>(wrapper, filters.get(PrivateProjectEvent.class), excludingParams));

        if (deletedResults != null) {
            List<PrivateProjectEvent> dtos = deletedResults.stream().map(this::createDtoWithDeletionDate).toList();

            dao.add(new RDBMSQuery<>(wrapper, filters.get(PrivateProjectEvent.class)), dtos);
        }
    }

    /**
     * Creates new DB records for each private project that was missed when created or renamed.
     * 
     * @param projectList The list of private projects to process
     * @param dao The dao service
     * @param filters The filters service
     */
    private void updateMissedCreationAndRenameEvents(List<GitlabProjectResponse> projectList, DefaultHibernateDao dao,
            FilterService filters) {
        RequestWrapper wrapper = new FlatRequestWrapper(URI.create("https://api.eclipse.org"));

        MultivaluedMap<String, String> includingParams = new MultivaluedHashMap<>();
        includingParams
                .put(GitEcaParameterNames.PROJECT_IDS.getName(), projectList.stream().map(p -> Integer.toString(p.getId())).toList());

        // Get by id since project ids never change
        List<PrivateProjectEvent> results = dao.get(new RDBMSQuery<>(wrapper, filters.get(PrivateProjectEvent.class), includingParams));

        if (results != null) {
            // Includes only projects with namespaces that don't exist or events not in DB
            List<PrivateProjectEvent> dtos = projectList
                    .stream()
                    .filter(p -> results
                            .stream()
                            .noneMatch(r -> r.getCompositeId().getProjectPath().equalsIgnoreCase(p.getPathWithNamespace())))
                    .map(this::createDto)
                    .toList();

            dao.add(new RDBMSQuery<>(wrapper, filters.get(PrivateProjectEvent.class)), dtos);
        }
    }

    /**
     * Takes a PrivateProjectEvent, copies it, updates the deletion date, and returns the copy.
     * 
     * @param event The event to update
     * @return a PrivateProjectEvent with an updated deletion date
     */
    private PrivateProjectEvent createDtoWithDeletionDate(PrivateProjectEvent event) {
        PrivateProjectEvent out = new PrivateProjectEvent(event.getCompositeId().getUserId(), event.getCompositeId().getProjectId(),
                event.getCompositeId().getProjectPath());

        out.setEFUsername(fetchUserName(event.getCompositeId().getUserId()));
        out.setCreationDate(event.getCreationDate());
        out.setDeletionDate(LocalDateTime.now());
        out.setParentProject(event.getParentProject());
        return out;
    }

    /**
     * Takes the received GitlabProjectResponse object and maps it to a PrivateProjectEvent DTO.
     * 
     * @param project received project data
     * @return A PrivateProjectEvent object
     */
    private PrivateProjectEvent createDto(GitlabProjectResponse project) {
        PrivateProjectEvent dto = new PrivateProjectEvent(project.getCreatorId(), project.getId(), project.getPathWithNamespace());

        dto.setEFUsername(fetchUserName(project.getCreatorId()));
        dto.setCreationDate(project.getCreatedAt().toLocalDateTime());

        if (project.getForkedFromProject() != null) {
            dto.setParentProject(project.getForkedFromProject().getId());
        }

        return dto;
    }

    /**
     * fetches a user's name using the given user ID.
     * 
     * @param userId the desried user's id
     * @return the username or null
     */
    private String fetchUserName(Integer userId) {
        Optional<GitlabUserResponse> response = cache
                .get(Integer.toString(userId), new MultivaluedHashMap<>(), GitlabUserResponse.class,
                        () -> api.getUserInfo(apiToken, userId))
                .data();
        return response.isPresent() ? response.get().getUsername() : null;
    }
}
