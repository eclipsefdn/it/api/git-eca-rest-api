/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.api.models;

import java.util.List;

import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.Repository;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Model response for /installations/repositories
 * 
 * @author Martin Lowe
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_GithubInstallationRepositoriesResponse.Builder.class)
public abstract class GithubInstallationRepositoriesResponse {

    public abstract List<Repository> getRepositories();

    public static Builder builder() {
        return new AutoValue_GithubInstallationRepositoriesResponse.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setRepositories(List<Repository> repository);

        public abstract GithubInstallationRepositoriesResponse build();
    }
}
