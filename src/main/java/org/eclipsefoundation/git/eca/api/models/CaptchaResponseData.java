/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.api.models;

import java.util.List;

import jakarta.annotation.Nullable;

import org.eclipsefoundation.git.eca.namespace.HCaptchaErrorCodes;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_CaptchaResponseData.Builder.class)
public abstract class CaptchaResponseData {

    public abstract boolean getSuccess();
    
    @Nullable
    @JsonProperty("error-codes")
    public abstract List<HCaptchaErrorCodes> getErrorCodes();

    public static Builder builder() {
        return new AutoValue_CaptchaResponseData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setSuccess(boolean success);

        @JsonProperty("error-codes")
        public abstract Builder setErrorCodes(@Nullable List<HCaptchaErrorCodes> errorCodes);

        public abstract CaptchaResponseData build();
    }
}
