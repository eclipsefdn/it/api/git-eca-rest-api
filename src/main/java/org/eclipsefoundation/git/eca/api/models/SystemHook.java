/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.api.models;

import java.time.ZonedDateTime;
import java.util.List;

import jakarta.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_SystemHook.Builder.class)
public abstract class SystemHook {

    public abstract ZonedDateTime getCreatedAt();

    public abstract ZonedDateTime getUpdatedAt();

    public abstract String getEventName();

    public abstract String getName();

    public abstract String getOwnerEmail();

    public abstract String getOwnerName();

    public abstract List<Owner> getOwners();

    public abstract String getPath();

    public abstract String getPathWithNamespace();

    public abstract Integer getProjectId();

    public abstract String getProjectVisibility();

    @Nullable
    public abstract String getOldPathWithNamespace();

    public static Builder builder() {
        return new AutoValue_SystemHook.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setCreatedAt(ZonedDateTime created);

        public abstract Builder setUpdatedAt(ZonedDateTime updated);

        public abstract Builder setEventName(String eventName);

        public abstract Builder setName(String name);

        public abstract Builder setOwnerEmail(String ownerEmail);

        public abstract Builder setOwnerName(String ownerName);

        public abstract Builder setOwners(List<Owner> owners);

        public abstract Builder setPath(String path);

        public abstract Builder setPathWithNamespace(String path);

        public abstract Builder setProjectId(Integer projectId);

        public abstract Builder setProjectVisibility(String visibility);

        public abstract Builder setOldPathWithNamespace(@Nullable String oldPath);

        public abstract SystemHook build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_SystemHook_Owner.Builder.class)
    public abstract static class Owner {
        public abstract String getName();

        public abstract String getEmail();

        public static Builder builder() {
            return new AutoValue_SystemHook_Owner.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setName(String name);

            public abstract Builder setEmail(String email);

            public abstract Owner build();
        }
    }
}
