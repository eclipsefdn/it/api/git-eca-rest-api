/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.api.models;

import java.time.ZonedDateTime;

import jakarta.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_GitlabProjectResponse.Builder.class)
public abstract class GitlabProjectResponse {

    public abstract Integer getId();

    public abstract String getPathWithNamespace();

    public abstract Integer getCreatorId();

    public abstract ZonedDateTime getCreatedAt();

    @Nullable
    public abstract ForkedProject getForkedFromProject();

    public static Builder builder() {
        return new AutoValue_GitlabProjectResponse.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setId(Integer id);

        @JsonProperty("path_with_namespace")
        public abstract Builder setPathWithNamespace(String path);

        @JsonProperty("creator_id")
        public abstract Builder setCreatorId(Integer creatorId);

        @JsonProperty("created_at")
        public abstract Builder setCreatedAt(ZonedDateTime created);

        @JsonProperty("forked_from_project")
        public abstract Builder setForkedFromProject(@Nullable ForkedProject project);

        public abstract GitlabProjectResponse build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_GitlabProjectResponse_ForkedProject.Builder.class)
    public abstract static class ForkedProject {

        public abstract Integer getId();

        public static Builder builder() {
            return new AutoValue_GitlabProjectResponse_ForkedProject.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setId(Integer id);

            public abstract ForkedProject build();
        }
    }
}
