/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.git.eca.api.models.GitlabProjectResponse;
import org.eclipsefoundation.git.eca.api.models.GitlabUserResponse;
import org.jboss.resteasy.reactive.RestResponse;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;

/**
 * Interface for interacting with the GitLab API. Used to fetch project data.
 */
@ApplicationScoped
@RegisterRestClient
public interface GitlabAPI {

    /**
     * Fetches data for a project using the projectId. The id and a token of adequate permissions is required.
     * 
     * @param privateToken the header token
     * @param projectId the project id
     * @return A GitlabProjectResponse object
     */
    @GET
    @Path("/projects/{id}")
    GitlabProjectResponse getProjectInfo(@HeaderParam("PRIVATE-TOKEN") String privateToken, @PathParam("id") int projectId);

    /**
     * Fetches data for private projects. A token of adequate permissions is required. Visibility should be set to "private" and per_page
     * should be set to 100 to minimize API calls.
     * 
     * @param privateToken the header token
     * @param visibility the project visibility
     * @param perPage the number of results per page
     * @return A Response containing a private project list
     */
    @GET
    @Path("/projects")
    RestResponse<List<GitlabProjectResponse>> getPrivateProjects(@BeanParam BaseAPIParameters baseParams,
            @HeaderParam("PRIVATE-TOKEN") String privateToken, @QueryParam("visibility") String visibility,
            @QueryParam("per_page") Integer perPage);

    /**
     * Fetches data for a user using the userId. The id and a token of adequate permissions is required.
     * 
     * @param privateToken the header token
     * @param userId the project id
     * @returnA A GitlabUserResponse object
     */
    @GET
    @Path("/users/{id}")
    GitlabUserResponse getUserInfo(@HeaderParam("PRIVATE-TOKEN") String privateToken, @PathParam("id") int userId);
}
