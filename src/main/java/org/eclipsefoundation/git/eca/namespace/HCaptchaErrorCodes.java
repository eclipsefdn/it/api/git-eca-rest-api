/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.namespace;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import io.quarkus.qute.TemplateEnum;

/**
 * Mapping of hCaptcha error codes to human readable messages
 * 
 * @author Martin Lowe
 *
 */
@TemplateEnum
public enum HCaptchaErrorCodes {

    MISSING_INPUT_SECRET("missing-input-secret", "The secret key is not set, cannot validate request."),
    INVALID_INPUT_SECRET("invalid-input-secret", "The secret key is invalid or malformed, cannot validate requests."),
    MISSING_INPUT_RESPONSE("missing-input-response", "The response parameter is missing."),
    INVALID_INPUT_RESPONSE("invalid-input-response", "The response parameter is invalid or malformed."),
    BAD_REQUEST("bad-request", "The request is invalid or malformed."),
    INVALID_OR_ALREADY_SEEN_RESPONSE("invalid-or-already-seen-response",
            "The response parameter has already been checked, or has another issue."),
    NOT_USING_DUMMY_PASSCODE("not-using-dummy-passcode", "You have used a testing sitekey but have not used its matching secret."),
    SITEKEY_SECRET_MISMATCH("sitekey-secret-mismatch", "The sitekey is not registered with the provided secret.");

    private String code;
    private String message;

    private HCaptchaErrorCodes(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @JsonCreator
    public static HCaptchaErrorCodes forValue(String value) {
        return Stream.of(values()).filter(err -> err.code.equalsIgnoreCase(value)).findFirst().orElse(null);
    }

    @JsonValue
    public String toValue() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
