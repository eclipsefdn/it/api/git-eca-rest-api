/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.namespace;

/** 
 * A Helper class used to centralize the incoming webhook header values
*/
public final class WebhookHeaders {
    
    public static final String GITLAB_EVENT = "X-Gitlab-Event";

    public static final String GITHUB_EVENT = "X-GitHub-Event";
    public static final String GITHUB_DELIVERY = "X-GitHub-Delivery";

    private WebhookHeaders() {

    }
}
