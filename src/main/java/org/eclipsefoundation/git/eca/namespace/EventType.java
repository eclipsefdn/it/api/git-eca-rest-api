/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.namespace;

import java.util.stream.Stream;

public enum EventType {
    PROJECT_CREATE, PROJECT_DESTROY, PROJECT_RENAME, UNSUPPORTED;

    public String getName() {
        return this.name().toLowerCase();
    }

    public static EventType getType(String name) {
        if(name == null) {
            return UNSUPPORTED;
        }

        return Stream.of(values()).filter(e -> name.equalsIgnoreCase(e.getName())).findFirst().orElse(UNSUPPORTED);
    }
}
