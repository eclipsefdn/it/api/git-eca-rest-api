/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.namespace;

import java.util.Arrays;
import java.util.List;

import org.eclipsefoundation.utils.namespace.UrlParameterNamespace;

import jakarta.inject.Singleton;

@Singleton
public final class GitEcaParameterNames implements UrlParameterNamespace {
    public static final String COMMIT_ID_RAW = "commit_id";
    public static final String SHA_RAW = "sha";
    public static final String SHAS_RAW = "shas";
    public static final String PROJECT_ID_RAW = "project_id";
    public static final String PROJECT_IDS_RAW = "project_ids";
    public static final String NOT_IN_PROJECT_IDS_RAW = "not_in_project_ids";
    public static final String REPO_URL_RAW = "repo_url";
    public static final String FINGERPRINT_RAW = "fingerprint";
    public static final String USER_ID_RAW = "user_id";
    public static final String PROJECT_PATH_RAW = "project_path";
    public static final String PARENT_PROJECT_RAW = "parent_project";
    public static final String STATUS_ACTIVE_RAW = "active";
    public static final String STATUS_DELETED_RAW = "deleted";
    public static final String SINCE_RAW = "since";
    public static final String UNTIL_RAW = "until";
    public static final String REPOSITORY_FULL_NAME_RAW = "repo_full_name";
    public static final String ORG_NAME_RAW = "org";
    public static final String REPOSITORY_NAME_RAW = "repo_name";
    public static final String INSTALLATION_ID_RAW = "installation_id";
    public static final String APPLICATION_ID_RAW = "application_id";
    public static final String LAST_UPDATED_BEFORE_RAW = "last_updated_before";
    public static final String PULL_REQUEST_NUMBER_RAW = "pull_request_number";
    public static final String USER_MAIL_RAW = "user_mail";
    public static final String NEEDS_REVALIDATION_RAW = "needs_revalidation";
    public static final UrlParameter COMMIT_ID = new UrlParameter(COMMIT_ID_RAW);
    public static final UrlParameter SHA = new UrlParameter(SHA_RAW);
    public static final UrlParameter SHAS = new UrlParameter(SHAS_RAW);
    public static final UrlParameter PROJECT_ID = new UrlParameter(PROJECT_ID_RAW);
    public static final UrlParameter PROJECT_IDS = new UrlParameter(PROJECT_IDS_RAW);
    public static final UrlParameter NOT_IN_PROJECT_IDS = new UrlParameter(NOT_IN_PROJECT_IDS_RAW);
    public static final UrlParameter REPO_URL = new UrlParameter(REPO_URL_RAW);
    public static final UrlParameter FINGERPRINT = new UrlParameter(FINGERPRINT_RAW);
    public static final UrlParameter USER_ID = new UrlParameter(USER_ID_RAW);
    public static final UrlParameter PROJECT_PATH = new UrlParameter(PROJECT_PATH_RAW);
    public static final UrlParameter PARENT_PROJECT = new UrlParameter(PARENT_PROJECT_RAW);
    public static final UrlParameter STATUS_ACTIVE = new UrlParameter(STATUS_ACTIVE_RAW);
    public static final UrlParameter STATUS_DELETED = new UrlParameter(STATUS_DELETED_RAW);
    public static final UrlParameter SINCE = new UrlParameter(SINCE_RAW);
    public static final UrlParameter UNTIL = new UrlParameter(UNTIL_RAW);
    public static final UrlParameter REPOSITORY_FULL_NAME = new UrlParameter(REPOSITORY_FULL_NAME_RAW);
    public static final UrlParameter REPOSITORY_NAME = new UrlParameter(REPOSITORY_NAME_RAW);
    public static final UrlParameter ORG_NAME = new UrlParameter(ORG_NAME_RAW);
    public static final UrlParameter INSTALLATION_ID = new UrlParameter(INSTALLATION_ID_RAW);
    public static final UrlParameter APPLICATION_ID = new UrlParameter(APPLICATION_ID_RAW);
    public static final UrlParameter LAST_UPDATED_BEFORE = new UrlParameter(LAST_UPDATED_BEFORE_RAW);
    public static final UrlParameter PULL_REQUEST_NUMBER = new UrlParameter(PULL_REQUEST_NUMBER_RAW);
    public static final UrlParameter USER_MAIL = new UrlParameter(USER_MAIL_RAW);
    public static final UrlParameter NEEDS_REVALIDATION = new UrlParameter(NEEDS_REVALIDATION_RAW);

    @Override
    public List<UrlParameter> getParameters() {
        return Arrays
                .asList(COMMIT_ID, SHA, SHAS, PROJECT_ID, PROJECT_IDS, NOT_IN_PROJECT_IDS, REPO_URL, FINGERPRINT, USER_ID, PROJECT_PATH,
                        PARENT_PROJECT, STATUS_ACTIVE, STATUS_DELETED, SINCE, UNTIL, REPOSITORY_FULL_NAME, ORG_NAME, REPOSITORY_NAME,
                        INSTALLATION_ID, APPLICATION_ID, LAST_UPDATED_BEFORE, PULL_REQUEST_NUMBER, USER_MAIL, NEEDS_REVALIDATION);
    }

}
