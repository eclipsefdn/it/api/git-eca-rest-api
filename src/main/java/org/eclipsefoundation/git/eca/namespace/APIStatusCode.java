/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.namespace;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonValue;

public enum APIStatusCode {
	SUCCESS_DEFAULT(200),
	SUCCESS_COMMITTER(201),
	SUCCESS_CONTRIBUTOR(202),
	SUCCESS_SKIPPED(203),
	ERROR_DEFAULT(-401),
	ERROR_SIGN_OFF(-402),
	ERROR_SPEC_PROJECT(-403),
	ERROR_AUTHOR(-404),
	ERROR_COMMITTER(-405),
	ERROR_PROXY_PUSH(-406),
	ERROR_COMMITTER_NOT_FOUND(-407),
	ERROR_AUTHOR_NOT_FOUND(-408);

	private int code;

	private APIStatusCode(int code) {
		this.code = code;
	}
	
	/**
	 * Checks if the associated error is a committer error or a general author error.
	 * 
	 * @return true if the current error is related to committer status, false otherwise
	 */
	public boolean isCommitterError() {
	    return this.equals(ERROR_COMMITTER) || this.equals(ERROR_COMMITTER_NOT_FOUND) || this.equals(ERROR_SPEC_PROJECT);
	}
	
	public static APIStatusCode getValueForCode(int code) {
	    return Stream.of(values()).filter(c -> c.code == code).findFirst().orElse(null);
	}

	@JsonValue
	public int getValue() {
		return code;
	}
}
