#!/usr/bin/env ruby
# encoding: UTF-8

## Simplified version of eca.rb, made to help debug what is being output by

require 'json'
require 'httparty'
require 'multi_json'

## Process the commit into a hash object that will be posted to the ECA validation service
def process_commit(sha)
  commit_parents_raw = `git show -s --format='%P' #{sha}`
  commit_parents = commit_parents_raw.split(/\s/)
  return {
    :author => {
      :name => `git show -s --format='%an' #{sha}`.force_encoding("utf-8"),
      :mail => `git show -s --format='%ae' #{sha}`.force_encoding("utf-8"),
    },
    :committer => {
      :name => `git show -s --format='%cn' #{sha}`.force_encoding("utf-8"),
      :mail => `git show -s --format='%ce' #{sha}`.force_encoding("utf-8"),
    },
    :body => `git show -s --format='%B' #{sha}`.force_encoding("utf-8"),
    :subject => `git show -s --format='%s' #{sha}`.force_encoding("utf-8"),
    :hash => `git show -s --format='%H' #{sha}`,
    :parents => commit_parents
  }
end

## Read in the arguments passed from GitLab and split them to an arg array
stdin_raw = ARGF.read;
stdin_args = stdin_raw.split(/\s+/)

## Set the vars for the commit hashes of current pre-receive event
previous_head_commit = stdin_args[0]
new_head_commit = stdin_args[1]

## Get the project ID from env var, extracting from pattern 'project-###'
project_id = ENV['GL_REPOSITORY'][8..-1]
project_path = ENV['GL_PROJECT_PATH']
## When pushing group wikis, project_path may be empty
if (project_path.nil?) then
  puts "Cannot retrieve project path, likely a group. Skipping validation"
  exit 0
end
## Check if current repo is a project wiki (no project ID and ends in .wiki)
if (project_id.nil? && project_path =~ WIKI_REGEX_MATCH) then
  puts "Repository is a project wiki and not bound by ECA, skipping"
  exit 0
end

## Get data about project from API
project_response = HTTParty.get("http://localhost/api/v4/projects/#{project_id}")
## Format data to be able to easily read and process it
project_json_data = MultiJson.load(project_response.body)
if (project_json_data.nil? || project_json_data.class.name == 'Array') then
  puts "Couldn't load project data, assumed non-tracked project and skipping validation."
  exit 0
end
## Get the web URL, checking if project is a fork to get original project URL
if (!project_json_data['forked_from_project'].nil?) then
  puts "Non-Eclipse project repository detected: ECA validation will be skipped.\n\nNote that any issues with sign off or committer access will be flagged upon merging into the main project repository."
  exit 0
else 
  project_url = project_json_data['web_url']
end

## Get all new commits for branch, relative to itself for existing branch, relative to tree for new
diff_git_commits_raw = ''
if (previous_head_commit =~ /0+/) then
  ## This isn't perfect as its relative to fork, but should be huge improvement
  diff_git_commits_raw = `git rev-list #{new_head_commit}  --not --branches=*`
else
  diff_git_commits_raw = `git rev-list #{previous_head_commit}...#{new_head_commit}`
end
diff_git_commits = diff_git_commits_raw.split(/\n/)

processed_git_data = []
diff_git_commits.each do |commit|
  processed_git_data.push(process_commit(commit))
end

## Create the JSON payload
json_data = {
  :repoUrl => project_url,
  :provider => 'gitlab',
  :commits => processed_git_data
}
puts MultiJson.dump(json_data).gsub(/(\\n|\\r)/, '')
